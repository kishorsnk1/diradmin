import { Component, OnInit, ViewChild, ElementRef, TemplateRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '@app/_models';
import { ToastrService } from 'ngx-toastr';
import { first } from 'rxjs/operators';
import { AccountService, AlertService } from '@app/_services';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-create-game',
  templateUrl: './create-game.component.html',
  styleUrls: ['./create-game.component.less']
})
export class CreateGameComponent implements OnInit {
	@ViewChild('createGame', { static: true }) createGame: TemplateRef<any>;

	user: User;
	form: FormGroup;
    loading = false;
    submitted = false;
    result:any = [];
    routeId: string;
    UserData:any=[];
    Role:any;
    modalReference = null;
    types = [{'name':'Type1'},{'name':'Type2'},{'name':'Type3'}];
    availableGames:any=[];
p = 1;
selectedAll: boolean;
selectedListGame: any = [];
gameidArray: any = [];
enableEditIndex = '';
    enableEdit = false;
    EnableBtn = false;
    filterargs:any;
	constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private accountService: AccountService,
        private alertService: AlertService,
        private toastr: ToastrService,
	    private modal: NgbModal,
	) {
	    this.user = this.accountService.userValue;
	    this.routeId = this.user.data[0].id;
	    this.Role = this.user.role;
	    
	}

  	ngOnInit(){

  		this.form = this.formBuilder.group({
          name: ['', Validators.required],
          description: ['', Validators.required],
          type: ['', Validators.required],
      });

      this.getGames();

  	}

  	createPopup(){
  		this.modalReference = this.modal.open(this.createGame, { size: 'lg' });
  	}

    getGames(){
      this.accountService.getAllGames().subscribe((response) => {
          this.availableGames = response;
      });
    }

    checkAll() {
      this.selectedAll = false;
      if (!this.selectedAll) {
        this.selectedAll = true;
      } else {
        this.selectedAll = false;
      }
    }

    onChange(row, isChecked: boolean) {

        let length = this.selectedListGame.length;
        let newarray: any = [];

        if (isChecked) {
          this.gameidArray.push(row);
        } else {
          const index = this.gameidArray.indexOf(row);
          this.gameidArray.splice(index, 1);
        }
        console.log(this.gameidArray);
    }

    enableEditMethod(e, id) {
      this.enableEdit = true;
      this.enableEditIndex = id;
      console.log(id, e);
    }

    updateRow(item){
      console.log(item);

      this.loading = true;

      this.loading = false;
    }

    deleteGame(){
      if(this.gameidArray.length == 0){
        this.toastr.error('Please select game');
      }else{
        console.log(this.gameidArray);
        this.toastr.success('Deleted successfully');
      }
      
    }

    get f() { return this.form.controls; }

    onSubmit(): void {

       this.submitted = true;
        //this.loading = true;
        if(!this.form.valid) {
          return;
        }
        this.loading = true;
        const formData = new FormData();

        formData.append('name', this.form.value.name);
        formData.append('description', this.form.value.description);
        formData.append('type', this.form.value.type);
        this.loading = false;
        console.log(formData);
        this.toastr.success('Game created successfully');
    }

    resetCreateGame(){
      this.form.reset();
    }

    editGame(){
      console.log(this.gameidArray);
    }

}
