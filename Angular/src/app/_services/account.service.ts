﻿import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import jwt_decode from "jwt-decode";
import { environment } from '@environments/environment';
import { User } from '@app/_models';

@Injectable({ providedIn: 'root' })
export class AccountService {
  private userSubject: BehaviorSubject<User>;
  public user: Observable<User>;

  constructor(
    private router: Router,
    private http: HttpClient
  ) {
        var oiUser = 
        {"data":[
      {
          "id": "1",
          "TeamId": "12",
          "CompanyId": "2",
          "Role": "1",
          "UserName": "Admin",
          "Email": "admin@gmail.com",
          "JobPosition": "Admin",
          "Phone": "9857898785",
          "IsActive": "1",
          "Password": "password",
          "token" : "asdasdsadadsdad"
      }
    ]}
    ;
    localStorage.setItem('user', JSON.stringify(oiUser));
    this.userSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('user')));
    this.user = this.userSubject.asObservable();

  }
  

  public get userValue(): User {
    //console.log(this.userSubject.value);
    return this.userSubject.value;
  }

  login(Email, Password) {
    return this.http.post<User>(`${environment.apiUrl}/user/login`, { Email, Password })
      .pipe(map(user => {
        // store user details and jwt token in local storage to keep user logged in between page refreshes
        //console.log('user', user.message);
        console.log('user', user);
        if (user.statuscode === '200') {
          localStorage.setItem('user', JSON.stringify(user));
          //localStorage.setItem('__hbvhfbvjvjdnvjjncjmkcmckm', JSON.stringify(user.token));
          this.userSubject.next(user);
        }
        return user;
      }));
  }

  loginTest(Email, Password) {
    return this.http.post<User>(`http://www.json-generator.com/api/json/get/cfhdPsUACW?indent=2`, { Email, Password }, { headers: new HttpHeaders({ "Content-Type": "application/json",'Access-Control-Allow-Origin': '*','Access-Control-Allow-Methods': 'POST' }) })
      .pipe(map(user => {
        // store user details and jwt token in local storage to keep user logged in between page refreshes
        //console.log('user', user.message);
        console.log('user', user);
        if (user.statuscode === '200') {
          localStorage.setItem('user', JSON.stringify(user));
          //localStorage.setItem('__hbvhfbvjvjdnvjjncjmkcmckm', JSON.stringify(user.token));
          this.userSubject.next(user);
        }
        return user;
      }));
  }

  loginforMentor(data) {
    return this.http.post<User>(`${environment.apiUrl}/user/loginforMentor`, data)
      .pipe(map(user => {
        // store user details and jwt token in local storage to keep user logged in between page refreshes
        //console.log('user', user.message);
        console.log('user', user);
        if (user.statuscode === '200') {
          localStorage.removeItem('user');
          localStorage.setItem('user', JSON.stringify(user));
          //localStorage.setItem('__hbvhfbvjvjdnvjjncjmkcmckm', JSON.stringify(user.token));
          this.userSubject.next(user);
        }
        return user;
      }));
  }

  logout() {
    //this.sessionDetails();
    // remove user from local storage and set current user to null
    localStorage.removeItem('user');
    localStorage.removeItem('__hbvhfbvjvjdnvjjncjmkcmckm');
    this.userSubject.next(null);
    this.router.navigate(['/account/login']);
  }

  forgotPassword(data){
    // console.log(data);
    return this.http.post(`${environment.apiUrl}/user/forgotpassword`, data);
  }
  resetPassword(data){
    // console.log(data);
    return this.http.post(`${environment.apiUrl}/user/resetPassword`, data);
  }

/*-- Token Start --*/
resolveToken(token) {
        if (token == "0") token = this.getToken();
        token = token == 0 ? null : token;
        if (token == null) {
            this.changeCurrentUser(0);
            // this.router.navigate(['/login'])
            return;
        } else {

            var user = this.getDecodedAccessToken(token);
console.log(user);
            //this.changeCurrentUser(user.data);
        }
    }
  getToken() {
        return localStorage.getItem("__hbvhfbvjvjdnvjjncjmkcmckm");
        // this.resolveToken(localStorage.getItem("token"));
    }
    removeToken() {
        localStorage.removeItem("__hbvhfbvjvjdnvjjncjmkcmckm");
        this.resolveToken(0)
    }
    storeToken(token) {
        if (token == "0") {
            localStorage.setItem("__hbvhfbvjvjdnvjjncjmkcmckm", null);
        } else {
            localStorage.setItem("__hbvhfbvjvjdnvjjncjmkcmckm", token);
            console.log(token)
            this.resolveToken(token);
        }
    }
    getDecodedAccessToken(token) {
        try {
            return jwt_decode(token);
        } catch (Error) {
          console.log(Error);
            return null;
        }
    }
    getTokenExpirationDate(token: string): Date {
        const decoded = jwt_decode(token);
        console.log(decoded);

        if (decoded.exp === undefined) return null;

        const date = new Date(0);
        date.setUTCSeconds(decoded.exp);
        return date;
    }
    isTokenExpired(token?: string): boolean {
        if (!token) token = this.getToken();
        if (!token) return true;

        const date = this.getTokenExpirationDate(token);
        if (date === undefined) return false;
        return !(date.valueOf() > new Date().valueOf());
    }
    changeCurrentUser(data) {
        if (data == 0) {
            localStorage.setItem("token", "0");
        }
        //this._current_user.next(data);
    }

    /*-- Token End --*/

  register(user: User) {
    return this.http.post(`${environment.apiUrl}/user/register`, user);
  }

  registerMember(data) {
    return this.http.post(`${environment.apiUrl}/user/registerMember`, data);
  }

  getAll() {
    return this.http.get<User[]>(`${environment.apiUrl}/user/allusers`);
  }

  getById(id: string) {
    return this.http.get<User>(`${environment.apiUrl}/user/${id}`);
  }

  validateEmail(email) {
      var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(email);
  }


  update(id, params) {
    return this.http.put(`${environment.apiUrl}/user/${id}`, params)
      .pipe(map(x => {
        // update stored user if the logged in user updated their own record
        if (id == this.userValue.id) {
          // update local storage
          const user = { ...this.userValue, ...params };
          localStorage.setItem('user', JSON.stringify(user));

          // publish updated user to subscribers
          this.userSubject.next(user);
        }
        return x;
      }));
  }

  delete(id: string) {
    return this.http.delete(`${environment.apiUrl}/user/${id}`)
      .pipe(map(x => {
        // auto logout if the logged in user deleted their own record
        if (id == this.userValue.id) {
          this.logout();
        }
        return x;
      }));
  }

  sessionDetails(data){
    //console.log(data.data);
    return this.http.post(`${environment.apiUrl}/user/logout`, data.data);
  }

  getAllGames(){
    return this.http.get(`./assets/json/creategame.json`);
  }

  changePassword(data){
    return this.http.post(`${environment.apiUrl}/changePassword`,data, { headers: new HttpHeaders({ "Content-Type": "application/json", 'Authorization': this.getToken() }) });
  }
}
