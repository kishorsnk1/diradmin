import { NgModule } from '@angular/core';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';

import { HomeComponent } from './home';
import { AuthGuard } from './_helpers';

import { UserManagementComponent } from './user-management/user-management.component'
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { ProfileComponent } from './profile/profile.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { GraphsCandidateComponent } from './graphs-candidate/graphs-candidate.component';
import { GraphsCourseComponent } from './graphs-course/graphs-course.component';
import { ReportsCandidateComponent } from './reports-candidate/reports-candidate.component';
import { ReportsCourseComponent } from './reports-course/reports-course.component';
import { CreateGameComponent } from './create-game/create-game.component';

const accountModule = () => import('./account/account.module').then(x => x.AccountModule);
const usersModule = () => import('./users/users.module').then(x => x.UsersModule);

const routes: Routes = [
    { path: '', component: HomeComponent, canActivate: [AuthGuard]},
    // { path: 'users', loadChildren: usersModule, canActivate: [AuthGuard] },
    { path: 'account', loadChildren: accountModule },
    { path: 'userManagement', component: UserManagementComponent, canActivate: [AuthGuard]},
    { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard]},
    { path: 'changePassword', component: ChangePasswordComponent, canActivate: [AuthGuard]},
    { path: 'resetPassword', component: ResetPasswordComponent },
    { path: 'GraphsCandidate', component: GraphsCandidateComponent, canActivate: [AuthGuard]},
    { path: 'GraphsCourse', component: GraphsCourseComponent, canActivate: [AuthGuard]},
    { path: 'ReportsCandidate', component: ReportsCandidateComponent, canActivate: [AuthGuard]},
    { path: 'ReportsCourse', component: ReportsCourseComponent, canActivate: [AuthGuard]},
    { path: 'CreateGame', component: CreateGameComponent, canActivate: [AuthGuard]},
    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes, { useHash: true })],
    exports: [RouterModule]
})
export class AppRoutingModule { }
