import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraphsCandidateComponent } from './graphs-candidate.component';

describe('GraphsCandidateComponent', () => {
  let component: GraphsCandidateComponent;
  let fixture: ComponentFixture<GraphsCandidateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraphsCandidateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraphsCandidateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
