import { Component, OnInit, ViewChild, AfterViewInit, ChangeDetectionStrategy, TemplateRef } from '@angular/core';
import { AccountService, AlertService } from '@app/_services';
import { User } from '@app/_models';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { first } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators, AbstractControl, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from '@environments/environment';
import {HashLocationStrategy, Location, LocationStrategy} from '@angular/common';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.less']
})
export class SidebarComponent implements OnInit {

public href: string = "";

  modalReference = null;
	currentUser:any = [];
  teamData:any = {};
  companyTeams:any = [];
  adminData:any = [];

  user: User;
  inviteByEmailForm: FormGroup;
  inviteByLinkForm: FormGroup;

  role:any;
  currentRole = '';
  teamName = '';
  CurrentUserName = '';  
  submitted = false;
  submittedLink = false;
  forbidden = true;
  invitationLink = '';
  loading = false;
  sidebarMenu:any =[];
  getFullRoute:any;
  activeLink:any;
  getActRoute:any;
  userId:any;
  teamId:any;
  ISThisOBJ = false;
  TeamId:any;
  MemberId:any;
  Role:any;
  loggedTeamId:any;
  rTeam = '';
  pageSlug:any;
  constructor(
          private accountService: AccountService,
          private modal: NgbModal,
          private formBuilder: FormBuilder,
          private route: ActivatedRoute,
          private router: Router,
          private alertService: AlertService,
          private toastr: ToastrService,
          private location: Location
        ) {
  	this.user = this.accountService.userValue;
  	this.role = this.user.role;
    this.CurrentUserName = this.user.data[0].UserName;
    this.userId = this.user.data[0].id;
    //console.log(this.userId);
    this.location = location;
    this.loggedTeamId = this.user.data[0].TeamId;

  }

  ngOnInit(){

    this.href = this.router.url;
        console.log(this.router.url);

    this.route.queryParams.subscribe(params => {
     if(params.ISThisOBJ != undefined){
        this.ISThisOBJ = true;
      }
    });
    this.getFullRoute = this.route;
    //console.log(this.router.url);
    var str = window.location.href;
    var Gn = str.includes("GraphsCandidate");
    var Gc = str.includes("GraphsCourse");
    var Rn = str.includes("ReportsCandidate");
    var Rc = str.includes("ReportsCourse");
    var Cg = str.includes("CreateGame");
    var Dg = str.includes("DecommissionGame");
    if(Gn){
      this.pageSlug = 'GraphsCandidate';
    }else if(Gc){
      this.pageSlug = 'GraphsCourse';
    }else if(Rn){
      this.pageSlug = 'ReportsCandidate';
    }else if(Rc){
      this.pageSlug = 'ReportsCourse';
    }else if(Cg){
      this.pageSlug = 'CreateGame';
    }else if(Dg){
      this.pageSlug = 'DecommissionGame';
    }else{
      this.pageSlug = '';
    }

  }

  routeURL(slug){
    this.pageSlug = slug;
  }


}
