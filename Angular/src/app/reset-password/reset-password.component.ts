import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { AccountService, AlertService } from '@app/_services';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.less']
})
export class ResetPasswordComponent implements OnInit {

      form: FormGroup;
    formforgot: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    errorMessage: string;
    loginMode = true;
    result:any = [];
    routeId:any;
    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private accountService: AccountService,
        private alertService: AlertService,        
        private toastr: ToastrService,
    ) { }

  ngOnInit() {
  	    

        this.route.queryParams.subscribe(params => {
	      //console.log(params);
	      this.routeId = params.e;
	      console.log(this.routeId);
	    });

	    this.form = this.formBuilder.group({
            email_id: [{'value':this.routeId,disabled:true}, Validators.required],
            password: ['', Validators.required]
        });

        this.formforgot = this.formBuilder.group({
            email_id: ['', Validators.required]
        });

        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

get f() { return this.form.controls; }
  onSubmit() {
        this.submitted = true;

        // reset alerts on submit
        this.alertService.clear();

        // stop here if form is invalid
        if (this.form.invalid) {
            return;
        }

        this.loading = true;

        this.accountService.resetPassword({'EMail':this.f.email_id.value,'Password':this.f.password.value})
          .subscribe((response:any) => {
            if (response.statuscode == 203) {
              this.toastr.error(response.msg);
              this.loading = false;
              console.log('data', response.msg);
              console.log('data', response.statuscode);
            } else{
              console.log(response);
              //this.router.navigateByUrl(this.returnUrl);
              this.accountService.storeToken(response.token);
              //this.router.navigate(['/']);
              window.location.replace(this.returnUrl);
              //this.router.navigate([this.returnUrl]);
            }
          });
    }

}
