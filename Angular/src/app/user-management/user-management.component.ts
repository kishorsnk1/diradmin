import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '@app/_models';
import { ToastrService } from 'ngx-toastr';
import { first } from 'rxjs/operators';
import { AccountService, AlertService } from '@app/_services';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-user-management',
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.less']
})
export class UserManagementComponent implements OnInit {
	user: User;
	companies:any = [];
	mentors:any = [];
    result:any = [];
    allocateData:any = [];
    allocatedMentors:any = [];
	activeNo = {};

  	p=1;
  	form: FormGroup;
    loading = false;
    submitted = false;
    sCompany:any;
    sMentor:any;
    choosenMentor:any;
    updateAllocation = false;
	constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private accountService: AccountService,
        private alertService: AlertService,
        private toastr: ToastrService
	) {
	    this.user = this.accountService.userValue;
	}

	ngOnInit(){
		if(this.user.role != '4'){
			this.router.navigate(['/']);
		}


		this.form = this.formBuilder.group({
            Name: ['', Validators.required],
            Email: ['', [Validators.required, Validators.email]],
            Phone: ['', [Validators.required, Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]],
            JobPosition: ['', Validators.required],
            Password: ['', [Validators.required, Validators.minLength(6)]],
            Role: ['2', Validators.required],
        });


	}




	get f() { return this.form.controls; }

   

    


}
