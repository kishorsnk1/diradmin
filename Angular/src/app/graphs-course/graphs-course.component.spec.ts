import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraphsCourseComponent } from './graphs-course.component';

describe('GraphsCourseComponent', () => {
  let component: GraphsCourseComponent;
  let fixture: ComponentFixture<GraphsCourseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraphsCourseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraphsCourseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
