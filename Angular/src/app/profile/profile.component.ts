import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '@app/_models';
import { ToastrService } from 'ngx-toastr';
import { first } from 'rxjs/operators';
import { AccountService, AlertService } from '@app/_services';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.less']
})
export class ProfileComponent implements OnInit {

	user: User;
	form: FormGroup;
    loading = false;
    submitted = false;
    result:any = [];
    routeId: string;
    UserData:any=[];
    Role:any;
  	constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private accountService: AccountService,
        private alertService: AlertService,
        private toastr: ToastrService
	) {
	    this.user = this.accountService.userValue;
	    this.routeId = this.user.data[0].id;
	    this.Role = this.user.role;
	    
	}

  	ngOnInit(){
  		if(this.Role == '1'){
	  		this.form = this.formBuilder.group({
	  			id: ['', Validators.required],
	            Name: ['', Validators.required],
	            Email: ['', [Validators.required, Validators.email]],
	            Phone: ['', [Validators.required, Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]],
	            Address: ['', Validators.required],
	            JobPosition: ['Admin', Validators.required],
	            TIN: ['', Validators.required],
	            FieldOfExpertise: ['', Validators.required],
	            NumberOfUsers: ['', Validators.required],
	            Role: ['', Validators.required]
	        });
	  	}else if(this.Role == '3' || this.Role == '2'){
	  		this.form = this.formBuilder.group({
	  			id: ['', Validators.required],
	            Name: ['', Validators.required],
	            Email: ['', [Validators.required, Validators.email]],
	            Phone: ['', [Validators.required, Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]],
	            Address: [''],
	            JobPosition: ['', Validators.required],
	            Role: ['', Validators.required]
	        });
	  	}



        
  	}

  	get f() { return this.form.controls; }

  	loadUserData(){
		console.log(this.UserData);
        if (this.UserData && this.Role == '1') { 
        	this.f.id.setValue(this.UserData[0].id);
        	this.f.Name.setValue(this.UserData[0].Name);
            this.f.Email.setValue(this.UserData[0].Email);
            this.f.Phone.setValue(this.UserData[0].Phone);
            this.f.Address.setValue(this.UserData[0].Address);
            this.f.TIN.setValue(this.UserData[0].TIN);
            this.f.FieldOfExpertise.setValue(this.UserData[0].FieldOfExpertise);
            this.f.NumberOfUsers.setValue(this.UserData[0].NumberOfUsers);
            this.f.Role.setValue(this.Role);
        }else if(this.UserData && this.Role == '3' || this.Role == '2') { 
        	this.f.id.setValue(this.UserData[0].id);
        	this.f.Name.setValue(this.UserData[0].UserName);
            this.f.Email.setValue(this.UserData[0].Email);
            this.f.Phone.setValue(this.UserData[0].Phone);
            this.f.JobPosition.setValue(this.UserData[0].JobPosition);
            this.f.Role.setValue(this.Role);
        }

    }

    onSubmit() {
        this.submitted = true;

        // reset alerts on submit
        this.alertService.clear();

        // stop here if form is invalid
        console.log(this.form);
        if (this.form.invalid) {
            return;
        }

        this.loading = true;

        
    }

}
