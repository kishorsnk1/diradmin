import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportsCandidateComponent } from './reports-candidate.component';

describe('ReportsCandidateComponent', () => {
  let component: ReportsCandidateComponent;
  let fixture: ComponentFixture<ReportsCandidateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportsCandidateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportsCandidateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
