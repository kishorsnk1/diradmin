import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '@app/_models';
import { ToastrService } from 'ngx-toastr';
import { first } from 'rxjs/operators';
import { AccountService, AlertService } from '@app/_services';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.less']
})
export class ChangePasswordComponent implements OnInit {

  	user: User;
	form: FormGroup;
    loading = false;
    submitted = false;
    result:any = [];
    routeId: string;
    UserData:any=[];
    Role:any;
  	constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private accountService: AccountService,
        private alertService: AlertService,
        private toastr: ToastrService
	) {
	    this.user = this.accountService.userValue;
	    this.routeId = this.user.data[0].id;
	    this.Role = this.user.role;
	    
	}

  	ngOnInit(){
  		this.form = this.formBuilder.group({
  			id: [this.routeId, Validators.required],
        	oldPassword: ['', [Validators.required, Validators.minLength(6)]],
        	newPassword: ['', [Validators.required, Validators.minLength(6)]],
    	});
  	}

  	get f() { return this.form.controls; }

  	onSubmit() {
        this.submitted = true;

        // reset alerts on submit
        this.alertService.clear();

        // stop here if form is invalid
        //console.log(this.form);
        if (this.form.invalid) {
            return;
        }

        this.loading = true;

        this.accountService.changePassword(this.form.value)
            .pipe(first())
            .subscribe(
                data => {
                    this.result = data;
                    //console.log(data);
                    if(this.result.statuscode == '200'){
                        this.toastr.success(this.result.msg);
                        this.loading = false;
                        this.form.reset();
                    }else{
                        this.toastr.error(this.result.msg);
                        this.loading = false;
                    }                    
                },
                error => {
                    this.toastr.error(error);
                    this.loading = false;
                });
    }

}
