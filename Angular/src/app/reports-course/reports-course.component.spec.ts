import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportsCourseComponent } from './reports-course.component';

describe('ReportsCourseComponent', () => {
  let component: ReportsCourseComponent;
  let fixture: ComponentFixture<ReportsCourseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportsCourseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportsCourseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
