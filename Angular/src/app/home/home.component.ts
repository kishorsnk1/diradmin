﻿import { Component, OnInit } from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { MultiDataSet, Label } from 'ng2-charts';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '@app/_models';
import { AccountService } from '@app/_services';
import { formatDate } from "@angular/common";
import { ToastrService } from 'ngx-toastr';

@Component({ templateUrl: 'home.component.html' })
export class HomeComponent {
  user: User;
  Role:any;

  companiesLength = 0;
  mentorsLength = 0;
  greet ='';
  daysleft:any;
  quarterleft:any;
  date:any;
  toDate:any;
  toDatequarter:any;
  teamData:any;
  totalObjectives = 0;
  totalTeams = 0;
  totalMembers = 0;
  allottedCompanies = 0;
  gotcompanies:any=[];
  emailID:any;
  thisisCompany:any;
  loading = false;
  returnUrl: string;
  constructor(
    private accountService: AccountService,
    private router: Router,
    private route: ActivatedRoute,
    private toastr: ToastrService
  ) {
    this.user = this.accountService.userValue;
    this.teamData = {'CompanyId': this.user.data[0].CompanyId};
    this.Role = this.user.role;
    if(this.Role == '2'){
      this.allottedCompanies = this.user.data[0].Companies.length;
      this.gotcompanies= this.user.data[0].Companies;
      this.emailID = this.user.data[0].Email;
      this.thisisCompany = this.user.data[0].Companies[0].CompanyId;
      console.log(this.gotcompanies);
    }
  }

  ngOnInit(){
    this.Role = this.user.role;
    /* Greerting Msg Start */
      let myDate = new Date();
      let hrs = myDate.getHours();

      if (hrs < 12)
          this.greet = 'Good Morning';
      else if (hrs >= 12 && hrs <= 17)
          this.greet = 'Good Afternoon';
      else if (hrs >= 17 && hrs <= 24)
          this.greet = 'Good Evening';     
    /* Greerting Msg End */

      var curr = new Date;
      var firstday = new Date(curr.setDate(curr.getDate() - curr.getDay()));
      var lastday = new Date(curr.setDate(curr.getDate() - curr.getDay()+6));
      var format = 'yyyy-MM-dd';
      var locale = 'en-US';
      var StartDate = formatDate(firstday, format, locale);
      var EndDate = formatDate(lastday, format, locale);

      // var date = new Date();
      // var diff = date.getDate() - date.getDay() + (date.getDay() === 0 ? -6 : 1);
      // var endDate = diff + 6; 
      // var StartDate = new Date(date.setDate(diff));  
      // var EndDate = new Date(date.setDate(endDate));


      this.date = new Date();

      this.toDate = new Date(EndDate);
      var day = Math.floor((this.toDate - this.date) / (1000 * 60 * 60 * 24));
      if(day < 0){
        this.daysleft = 0;
      }else{
        this.daysleft = day;
      }
      //console.log(this.daysleft);

      /* For Quarter */
      var ds = new Date();
      var quarter = Math.floor((ds.getMonth() / 3));
      var firstDate = new Date(ds.getFullYear(), quarter * 3, 1);
      var endDates = new Date(firstDate.getFullYear(), firstDate.getMonth() + 3, 0);
      //this.datequarter = new Date(firstDate);
      this.toDatequarter = new Date(endDates);
      this.quarterleft = Math.floor((this.toDatequarter - this.date) / (1000 * 60 * 60 * 24)/7);
      //console.log(this.quarterleft);
  }

 

}
