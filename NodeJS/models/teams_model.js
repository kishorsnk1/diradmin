var db = require('../config/database');
var config = require('../config/general');
var md5 = require('md5');
var jwt = require('jsonwebtoken');
var common = require('../models/common');
var app = require('../models/app_model');
var dateFormat = require('dateformat');


module.exports.createTeam = function(data, uData, callback) {
    //console.log('data',data);
    q = 'SELECT Id FROM team WHERE CompanyId="' + data.CompanyId + '" AND UserId="' + data.UserId + '" AND TeamName="' + data.TeamName + '"';
    db.query(q, data, function(err, teams, fields) {
        if (err)
            return callback({ "statuscode": "203", "status": "error", "msg": "Error occurered while creating team", "error": err });
        else {
            if (teams.length < 1) {
                q = "INSERT INTO team set ? ";
                db.query(q, data, function(err, result, fields) {
                    if (err) return callback({ "statuscode": "203", "status": "error", "msg": "Error occurered while creating team", "error": err });
                    else {
                        /*app.getTeamsTree('u', data.UserId, (d) => {
                            console.log('d',d);
                            if (d.statuscode == 200 && d.status == 'success') {
                                return callback({ "statuscode": "200", "status": "success", "teams": d.data.teams, "msg": "Team added successfully" });
                            } else {
                                return callback({ "statuscode": "200", "status": "success", "teams": [], "msg": "Team added successfully" });
                            }
                        })*/
                        return callback({ "statuscode": "200", "status": "success", "msg": "Team added successfully" });
                    }
                });
            } else {
                return callback({ "returnMSG": 'TNE', "statuscode": "203", "status": "failed", "msg": "Team Name already exists try different one.." });
            }
        }
    });
}

module.exports.removeTeam = function(data, uData, callback) {
    //console.log('data',data);
    q = 'SELECT * FROM members WHERE TeamId="' + data.teamId + '"';
    db.query(q, data, function(err, teams, fields) {
        if (err)
            return callback({ "statuscode": "203", "status": "error", "msg": "Error occurered while deleting team", "error": err });       
        else {
            if (teams.length < 1) {
                q = "UPDATE `team` SET IsActive='0' WHERE id='" + data.teamId + "' ";
                db.query(q, function(e, teams, fields) {
                        if (e) {
                            return callback({
                                "statuscode": "203",
                                "status": "error",
                                "msg": 'something went wrong',
                                "error": e
                            })
                        } else {
                            return callback({
                                "statuscode": "200",
                                "status": "success",
                                "msg": 'Team deleted successfully',
                                "data": teams
                            })
                        }
                })  
            } else {
                return callback({ "returnMSG": 'TNE', "statuscode": "203", "status": "failed", "msg": "Team has members cant deleting team" });
            }
        }
    });
}


module.exports.getTeams = function(data, uData, callback) {
    q = "SELECT * FROM team WHERE CompanyId = '"+data.CompanyId+"' AND  IsActive='1'";
    db.query(q, function(e, teams, fields) {
            if (e) {
                return callback({
                    "statuscode": "203",
                    "status": "error",
                    "msg": 'something went wrong',
                    "error": e
                })
            } else {
                return callback({
                    "statuscode": "200",
                    "status": "success",
                    "msg": 'Success',
                    "data": teams
                })
            }
    })         
}

module.exports.inviteMembers = (data, callback) => {
    common.sendMail('invite_member', data, (d) => {
        //console.log(d);
        return callback({
            "returnMSG": 'ISS',
            "statuscode": "200",
            "status": "success",
            "msg": 'Invitations are sent successfully'
        });
    })
}




module.exports.addObjective = function(data, uData, callback) {
    //console.log(data);
    q = "INSERT INTO `objectives` (`ObjectiveName`, `CompanyId`, `TeamId`, `MemberId`, `ObjectiveType`, `DueQuarter`, `DueYear`, `Status`, `CreatedBy`, `CreatedDate`) VALUES ('"+data.ObjectiveName+"','"+data.CompanyId+"','"+data.TeamId+"','"+data.MemberId+"','"+data.ObjectiveType+"','"+data.DueQuarter+"','"+data.DueYear+"','"+data.Status+"','"+data.CreatedBy+"','"+ dateFormat(new Date(), 'yyyy-mm-dd') +"')";
    //console.log(q);
    db.query(q, function(e, teams, fields) {
            if (e) {
                return callback({
                    "statuscode": "203",
                    "status": "error",
                    "msg": 'something went wrong',
                    "error": e
                })
            } else {
                return callback({
                    "statuscode": "200",
                    "status": "success",
                    "msg": 'Objective created successfully',
                    "data": teams
                })
            }
    })
}

module.exports.updateObjective = function(data, uData, callback) {
    //console.log(data);

    if(data.DueQuarter == undefined && data.DueYear == undefined){
        q = "UPDATE `objectives` SET ObjectiveName='"+ data.ObjectiveName +"' WHERE id='" + data.objid + "' ";
    }else{            
        q = "UPDATE `objectives` SET DueYear='"+ data.DueYear +"', DueQuarter='"+ data.DueQuarter +"' WHERE id='" + data.objid + "' ";
    }

    db.query(q, function(e, objects, fields) {
        if (e) {
            return callback({
                "statuscode": "203",
                "status": "error",
                "msg": 'something went wrong',
                "error": e
            })
        } else {
            //console.log(objects);
            q = "SELECT * FROM `objectives` WHERE id='"+data.objid+"'";
            db.query(q, function(e, Singleobjects, fields) {
                if (e) {
                    return callback({
                        "statuscode": "203",
                        "status": "error",
                        "msg": 'something went wrong',
                        "error": e
                    })
                } else {
                    var sObj = Singleobjects;
                    if(sObj[0].AssignedByOkr != 0){
                        if(data.DueQuarter == undefined && data.DueYear == undefined){
                            q = "UPDATE `keyresults` SET KeyResultName='"+ data.ObjectiveName +"' WHERE id='" + sObj[0].AssignedByOkr + "' ";
                            db.query(q, function(e, keyresults, fields) {
                                if (e) {
                                    return callback({
                                        "statuscode": "203",
                                        "status": "error",
                                        "msg": 'something went wrong',
                                        "error": e
                                    })
                                } else {
                                    
                                }
                            })

                        }
                    }
                    return callback({
                        "statuscode": "200",
                        "status": "success",
                        "msg": 'Objective updated successfully',
                        "data": objects
                    })
                }
            })

            
        }
    })
    
}

module.exports.deleteObjective = function(data, uData, callback) {
    //console.log(data);
    q = "UPDATE `objectives` SET IsActive = 0 WHERE id='" + data.objid + "' ";
    //console.log(q);
    db.query(q, function(e, teams, fields) {
            if (e) {
                return callback({
                    "statuscode": "203",
                    "status": "error",
                    "msg": 'something went wrong',
                    "error": e
                })
            } else {
                return callback({
                    "statuscode": "200",
                    "status": "success",
                    "msg": 'Objective deleted successfully',
                    "data": teams
                })
            }
    })
}

module.exports.deleteMemberStatus = function(data, uData, callback) {

    q = "UPDATE users,members SET users.IsActive='"+ data.IsActive +"', members.IsActive='"+ data.IsActive +"' WHERE users.id = members.UserId AND members.UserId='" + data.id + "'";

    db.query(q, function(e, users, fields) {
            if (e) {
                return callback({
                    "statuscode": "203",
                    "status": "error",
                    "msg": 'something went wrong',
                    "error": e
                })
            } else {
                return callback({
                    "statuscode": "200",
                    "status": "success",
                    "msg": 'Member deleted successfully'
                })
            }
    })
}


module.exports.updateTargetValue = function(data, uData, callback) {
    //console.log(data);
    q = "UPDATE `keyresults` SET TargetValue = '"+data.TargetValue+"',Type = '"+data.Type+"', StartValue = '"+data.StartValue+"',Decimals = '"+data.Decimals+"' WHERE id='" + data.okrId + "' AND IsActive = '1'";
    //console.log(q);
    db.query(q, function(e, teams, fields) {
            if (e) {
                return callback({
                    "statuscode": "203",
                    "status": "error",
                    "msg": 'something went wrong',
                    "error": e
                })
            } else {
                qs = "SELECT * FROM `keyresults` WHERE ObjectiveId = '"+data.objId+"' AND IsActive = '1'";
                db.query(qs, function(e, output, fields) {
                    if (e) {
                        return callback({
                            "statuscode": "203",
                            "status": "error",
                            "msg": 'something went wrong',
                            "error": e
                        })
                    } else {

                        /*var totalOKR = 0;
                        var avgOkr = 0;
                        for(i=0;i<output.length;i++){                            
                            if(output[i].TargetValue < 100){
                                totalOKR += parseFloat((output[i].ProgressValue/output[i].TargetValue)*100);
                            }else{
                                totalOKR += parseFloat(output[i].ProgressValue);
                            }
                        }
                        var objTotal = Math.round(totalOKR/output.length);*/
                        var OKRFinalValue = 0;
                        for(i=0;i<output.length;i++){
                            OKRFinalValue += parseFloat((output[i].ProgressValue/output[i].TargetValue)*output[i].Weightage);
                            //console.log(OKRFinalValue);
                        }
                        
                        qz = "UPDATE `objectives` SET ProgressValue = '" + Math.round(OKRFinalValue) + "' WHERE id='" + data.objId + "' AND IsActive = '1'";
                        db.query(qz, function(e, res, fields) {
                            if (e) {
                                return callback({
                                    "statuscode": "203",
                                    "status": "error",
                                    "msg": 'something went wrong',
                                    "error": e
                                })
                            } else {
                                       
                                    return callback({
                                        "statuscode": "200",
                                        "status": "success",
                                        "msg": 'TargetValue Updated',
                                        "data": res
                                    })
                                   
                            }
                        })
                    }
                })
            }
    })
}

module.exports.updateWeightage = function(data, uData, callback) {
    //console.log(data);
    q = "UPDATE `keyresults` SET Weightage = '"+data.Weightage+"' WHERE id='" + data.okrId + "' ";
    //console.log(q);
    db.query(q, function(e, teams, fields) {
            if (e) {
                return callback({
                    "statuscode": "203",
                    "status": "error",
                    "msg": 'something went wrong',
                    "error": e
                })
            } else {
                qs = "SELECT * FROM `keyresults` WHERE ObjectiveId = '"+data.objId+"' AND IsActive = '1'";
                db.query(qs, function(e, output, fields) {
                    if (e) {
                        return callback({
                            "statuscode": "203",
                            "status": "error",
                            "msg": 'something went wrong',
                            "error": e
                        })
                    } else {

                        if(output.length > 1){
                            var leftWeightage = 100 - parseFloat(data.Weightage);
                            var leftOkr = output.length- 1;
                            var weighatgePerOkr = parseFloat(leftWeightage/leftOkr);
                            for(i=0;i<output.length;i++){

                                if(data.okrId != output[i].id){
                                    qw = "UPDATE `keyresults` SET Weightage = '"+weighatgePerOkr+"' WHERE id='" + output[i].id + "' ";
                                    db.query(qw, function(e, outputs, fields) {
                                        if (e) {
                                            return callback({
                                                "statuscode": "203",
                                                "status": "error",
                                                "msg": 'something went wrong',
                                                "error": e
                                            })
                                        }else{
                                            
                                        }
                                    })
                                }

                            }

                            qp = "SELECT * FROM `keyresults` WHERE ObjectiveId = '"+data.objId+"' AND IsActive = '1'";
                            db.query(qp, function(e, keys, fields) {
                                if (e) {
                                    return callback({
                                        "statuscode": "203",
                                        "status": "error",
                                        "msg": 'something went wrong',
                                        "error": e
                                    })
                                }else{
                                    var OKRFinalValue = 0;
                                    for(i=0;i<keys.length;i++){
                                        OKRFinalValue += parseFloat((keys[i].ProgressValue/keys[i].TargetValue)*keys[i].Weightage);
                                    }

                                    if(OKRFinalValue > 100){
                                        OKRFinalValue = 100;
                                    }

                                    qv = "UPDATE `objectives` SET ProgressValue = '"+Math.round(OKRFinalValue)+"' WHERE id='" + data.objId + "' AND IsActive = '1'";
                                    db.query(qv, function(e, objs, fields) {
                                        if (e) {
                                            return callback({
                                                "statuscode": "203",
                                                "status": "error",
                                                "msg": 'something went wrong',
                                                "error": e
                                            })
                                        }else{
                                            return callback({
                                                "statuscode": "200",
                                                "status": "success",
                                                "msg": 'Weightage Updated',
                                                "data": objs
                                            })
                                        }
                                    })
                                }
                            })

                        }else{
                            qp = "SELECT * FROM `keyresults` WHERE ObjectiveId = '"+data.objId+"' AND IsActive = '1'";
                            db.query(qp, function(e, keys, fields) {
                                if (e) {
                                    return callback({
                                        "statuscode": "203",
                                        "status": "error",
                                        "msg": 'something went wrong',
                                        "error": e
                                    })
                                }else{
                                    var OKRFinalValue = 0;
                                    for(i=0;i<keys.length;i++){
                                        OKRFinalValue += parseFloat((keys[i].ProgressValue/keys[i].TargetValue)*keys[i].Weightage);
                                    }

                                    qv = "UPDATE `objectives` SET ProgressValue = '"+Math.round(OKRFinalValue)+"' WHERE id='" + data.objId + "' AND IsActive = '1'";
                                    db.query(qv, function(e, objs, fields) {
                                        if (e) {
                                            return callback({
                                                "statuscode": "203",
                                                "status": "error",
                                                "msg": 'something went wrong',
                                                "error": e
                                            })
                                        }else{
                                            return callback({
                                                "statuscode": "200",
                                                "status": "success",
                                                "msg": 'Weightage Updated',
                                                "data": objs
                                            })
                                        }
                                    })
                                }
                            })
                        }

                        /*var totalOKR = 0;
                        var avgOkr = 0;
                        for(i=0;i<output.length;i++){                            
                            if(output[i].TargetValue < 100){
                                totalOKR += parseFloat((output[i].ProgressValue/output[i].TargetValue)*100);
                            }else{
                                totalOKR += parseFloat(output[i].ProgressValue);
                            }
                        }
                        var objTotal = Math.round(totalOKR/output.length);
                        qz = "UPDATE `objectives` SET ProgressValue = '" + objTotal + "' WHERE id='" + data.objId + "' ";
                        db.query(qz, function(e, res, fields) {
                            if (e) {
                                return callback({
                                    "statuscode": "203",
                                    "status": "error",
                                    "msg": 'something went wrong',
                                    "error": e
                                })
                            } else {
                                       
                                    return callback({
                                        "statuscode": "200",
                                        "status": "success",
                                        "msg": 'TargetValue Updated',
                                        "data": res
                                    })
                                   
                            }
                        })*/
                    }
                })
            }
    })
}

module.exports.getUserByObjective = function(data, uData, callback) {
    //console.log(data);
    q = "SELECT * FROM `objectives` WHERE id = '"+data.objId+"'";
    //console.log(q);
    db.query(q, function(e, teams, fields) {
            if (e) {
                return callback({
                    "statuscode": "203",
                    "status": "error",
                    "msg": 'something went wrong',
                    "error": e
                })
            } else {
                return callback({
                    "statuscode": "200",
                    "status": "success",
                    "msg": 'Success',
                    "data": teams
                })
            }
    })
}

module.exports.UnlinkFromObjective = function(data, uData, callback) {
    var q,qs;
    /*if(data.userType == 'member'){
        q = "UPDATE `keyresults` SET ProgressValue = '0', AssignedToObj = '0', AssignedToUserType = '', AssignedToUser = '0' WHERE id='" + data.okrId + "' ";

        qs = "UPDATE `objectives` SET AssignedByOkr = '0', AssignedByOkrUserType = '0', AssignByOkrUserId = '0' WHERE AssignedByOkr='" + data.okrId + "' ";
    }else if(data.userType == 'team'){
        q = "UPDATE `keyresults` SET ProgressValue = '0', AssignedToObj = '0', AssignedToUserType = '', AssignedToUser = '0' WHERE id='" + data.okrId + "' ";

        qs = "UPDATE `objectives` SET AssignedByOkr = '0', AssignedByOkrUserType = '0', AssignByOkrUserId = '0' WHERE AssignedByOkr='" + data.okrId + "' ";
    }else if(data.userType == 'company'){
        q = "UPDATE `keyresults` SET ProgressValue = '0', AssignedToObj = '0', AssignedToUserType = '', AssignedToUser = '0' WHERE id='" + data.okrId + "' ";

        qs = "UPDATE `objectives` SET AssignedByOkr = '0', AssignedByOkrUserType = '0', AssignByOkrUserId = '0' WHERE AssignedByOkr='" + data.okrId + "' ";
    }*/
    q = "UPDATE `keyresults` SET ProgressValue = '0', AssignedToObj = '0', AssignedToUserType = '', AssignedToUser = '0' WHERE id='" + data.okrId + "' ";

    qs = "UPDATE `objectives` SET AssignedByOkr = '0', AssignedByOkrUserType = '0', AssignByOkrUserId = '0' WHERE AssignedByOkr='" + data.okrId + "' ";
    //console.log(q,qs);
    db.query(q, function(e, res, fields) {
            if (e) {
                return callback({
                    "statuscode": "203",
                    "status": "error",
                    "msg": 'something went wrong',
                    "error": e
                })
            } else {
                db.query(qs, function(e, res, fields) {
                    if (e) {
                        return callback({
                            "statuscode": "203",
                            "status": "error",
                            "msg": 'something went wrong',
                            "error": e
                        })
                    } else {

                    }

                })
                return callback({
                    "statuscode": "200",
                    "status": "success",
                    "msg": 'Unlinked from objective',
                    "data": res
                })
            }
    })
}

module.exports.UnlinkFromKeyResult = function(data, uData, callback) {
    var q,qs;
    /*if(data.userType == 'member'){
        q = "UPDATE `keyresults` SET ProgressValue = '0', AssignedByObj = '0', AssignedByUser = '0', AssignedByUserType = '', IsActive ='0' WHERE id='" + data.objId + "' ";

        qs = "UPDATE `objectives` SET AssignedToOkr = '0', AssignedToOkrUserType = '0', AssignToOkrUserId = '0' WHERE AssignedToOkr='" + data.objId + "' ";
    }else if(data.userType == 'team'){
        q = "UPDATE `keyresults` SET ProgressValue = '0', AssignedByObj = '0', AssignedByUser = '0', AssignedByUserType = '', IsActive ='0' WHERE id='" + data.objId + "' ";

        qs = "UPDATE `objectives` SET AssignedToOkr = '0', AssignedToOkrUserType = '0', AssignToOkrUserId = '0' WHERE AssignedToOkr='" + data.objId + "' ";
    }else if(data.userType == 'company'){
        q = "UPDATE `keyresults` SET ProgressValue = '0', AssignedByObj = '0', AssignedByUser = '0', AssignedByUserType = '', IsActive ='0' WHERE id='" + data.objId + "' ";

        qs = "UPDATE `objectives` SET AssignedToOkr = '0', AssignedToOkrUserType = '0', AssignToOkrUserId = '0' WHERE AssignedToOkr='" + data.objId + "' ";
    }*/
    q = "UPDATE `keyresults` SET ProgressValue = '0', AssignedByObj = '0', AssignedByUser = '0', AssignedByUserType = '', IsActive ='0' WHERE id='" + data.objId + "' ";

    qs = "UPDATE `objectives` SET AssignedToOkr = '0', AssignedToOkrUserType = '0', AssignToOkrUserId = '0' WHERE AssignedToOkr='" + data.objId + "' ";
    //console.log(q,qs);
    db.query(q, function(e, res, fields) {
            if (e) {
                return callback({
                    "statuscode": "203",
                    "status": "error",
                    "msg": 'something went wrong',
                    "error": e
                })
            } else {
                db.query(qs, function(e, res, fields) {
                    if (e) {
                        return callback({
                            "statuscode": "203",
                            "status": "error",
                            "msg": 'something went wrong',
                            "error": e
                        })
                    } else {

                    }

                })
                return callback({
                    "statuscode": "200",
                    "status": "success",
                    "msg": 'Unlinked from key result',
                    "data": res
                })
            }
    })
}

module.exports.assignOkrAsObjective = function(data, uData, callback) {
    //console.log(data);
    q = "INSERT INTO `objectives` (`ObjectiveName`, `CompanyId`, `TeamId`, `MemberId`,`AssignedByOkr`,`AssignedByOkrUserType`,`AssignByOkrUserId`, `ObjectiveType`, `DueQuarter`, `DueYear`, `Status`,`ProgressValue`, `CreatedBy`, `CreatedDate`) VALUES ('"+data.ObjectiveName+"','"+data.CompanyId+"','"+data.TeamId+"','"+data.MemberId+"','"+data.AssignedByOkr+"','"+data.AssignedByUserType+"','"+data.AssignedByUser+"','"+data.ObjectiveType+"','"+data.DueQuarter+"','"+data.DueYear+"','"+data.Status+"','0','"+data.CreatedBy+"','"+ dateFormat(new Date(), 'yyyy-mm-dd') +"')";
    //console.log(q);
    db.query(q, function(e, obj, fields) {
        if (e) {
            return callback({
                "statuscode": "203",
                "status": "error",
                "msg": 'something went wrong',
                "error": e
            })
        } else {
            //console.log('obs',obj);
            q = "SELECT * FROM `objectives` WHERE id = '"+obj.insertId+"'";
            //console.log(q);
            db.query(q, function(e, res, fields) {
                if (e) {
                    return callback({
                        "statuscode": "203",
                        "status": "error",
                        "msg": 'something went wrong',
                        "error": e
                    })
                } else {
                    //console.log(res);
                    if(data.ObjectiveType == '1'){
                        //company
                        q = "UPDATE `keyresults` SET ProgressValue = '0', AssignedToObj = '"+obj.insertId+"', AssignedToUserType = 1, AssignedToUser = '"+data.CompanyId+"' WHERE id='" + data.AssignedByOkr + "' ";
                    }else if(data.ObjectiveType == '3'){
                        // member
                        q = "UPDATE `keyresults` SET ProgressValue = '0', AssignedToObj = '"+obj.insertId+"', AssignedToUserType = 3, AssignedToUser = '"+data.MemberId+"' WHERE id='" + data.AssignedByOkr + "' ";
                    }else if(data.ObjectiveType == '2'){
                        //team
                        q = "UPDATE `keyresults` SET ProgressValue = '0', AssignedToObj = '"+obj.insertId+"', AssignedToUserType = 2, AssignedToUser = '"+data.TeamId+"' WHERE id='" + data.AssignedByOkr + "' ";
                    }
                    //console.log(q);
                    db.query(q, function(e, result, fields) {
                        if (e) {
                            return callback({
                                "statuscode": "203",
                                "status": "error",
                                "msg": 'something went wrong',
                                "error": e
                            })
                        } else {
                            //console.log();
                            /* Update Objective Progress Value Starts */
                            qs = "SELECT * FROM `keyresults` WHERE id = '"+data.AssignedByOkr+"' AND IsActive = '1'";
                            db.query(qs, function(e, keyObjid, fields) {
                                if (e) {
                                    return callback({
                                        "statuscode": "203",
                                        "status": "error",
                                        "msg": 'something went wrong',
                                        "error": e
                                    })
                                } else {

                                    var GetObjId = keyObjid[0].ObjectiveId;
                                    qs = "SELECT * FROM `keyresults` WHERE ObjectiveId = '"+GetObjId+"' AND IsActive = '1'";
                                    db.query(qs, function(e, output, fields) {
                                        if (e) {
                                            return callback({
                                                "statuscode": "203",
                                                "status": "error",
                                                "msg": 'something went wrong',
                                                "error": e
                                            })
                                        } else {
                                                var OKRFinalValue = 0;
                                                for(i=0;i<output.length;i++){
                                                    OKRFinalValue += parseFloat((output[i].ProgressValue/output[i].TargetValue)*output[i].Weightage);
                                                    //console.log(OKRFinalValue);
                                                }
                                                if(OKRFinalValue > 100){
                                                    OKRFinalValue = 100;
                                                }

                                                qz = "UPDATE `objectives` SET ProgressValue = '" + Math.round(OKRFinalValue) + "' WHERE id='" + GetObjId + "' AND IsActive = '1'";
                                                db.query(qz, function(e, res, fields) {
                                                    if (e) {
                                                        return callback({
                                                            "statuscode": "203",
                                                            "status": "error",
                                                            "msg": 'something went wrong',
                                                            "error": e
                                                        })
                                                    } else { 
                                                        return callback({
                                                            "statuscode": "200",
                                                            "status": "success",
                                                            "msg": 'OKR assigned successfully',
                                                            "data": res
                                                        })

                                                    }
                                                }) 
                                        }
                                    })
                                }
                            })

                            /* Update Objective Progress Value Ends */



                            
                        }
                    })
                }
            })
        }
    })
}


module.exports.assignObjectiveAsOkr = function(data, uData, callback) {
    //console.log(data);
    q = "INSERT INTO `keyresults` (`KeyResultName`, `ObjectiveId`, `Status`, `ProgressValue`,`AssignedByObj`,`AssignedByUser`,`AssignedByUserType`,`CreatedBy`, `CreatedDate`) VALUES ('"+data.KeyResultName+"','"+data.ObjectiveId+"','"+data.Status+"','"+data.ProgressValue+"','"+data.AssignedByObj+"','"+data.userIds+"','"+data.ObjectiveTypeBy+"','"+data.CreatedBy+"','"+ dateFormat(new Date(), 'yyyy-mm-dd') +"')";
    //console.log('qa',q);
    db.query(q, function(e, obj, fields) {
            if (e) {
                return callback({
                    "statuscode": "203",
                    "status": "error",
                    "msg": 'something went wrong',
                    "error": e
                })
            } else {
                //console.log('obs',obj);
                q = "SELECT * FROM `keyresults` WHERE id = '"+obj.insertId+"'";
                //console.log(q);
                db.query(q, function(e, res, fields) {
                        if (e) {
                            return callback({
                                "statuscode": "203",
                                "status": "error",
                                "msg": 'something went wrong',
                                "error": e
                            })
                        } else {
                            //console.log(res);
                            var userTypes = '';
                            var userIds = '';
                            var qs = '';
                            if(data.ObjectiveType == '1'){
                                qs = "SELECT o.id,o.ObjectiveName,k.id as KeyId, k.KeyResultName,o.CompanyId,c.Name AS CompanyName FROM `objectives` o  INNER JOIN `keyresults` k ON k.ObjectiveId = o.id INNER JOIN `company` c ON c.id=o.CompanyId WHERE k.id = '"+obj.insertId+"'";
                                userTypes = '1';
                                
                            }else if(data.ObjectiveType == '2'){
                                qs = "SELECT o.id,o.ObjectiveName,k.id as KeyId, k.KeyResultName,o.TeamId,t.TeamName FROM `objectives` o INNER JOIN `keyresults` k ON k.ObjectiveId = o.id INNER JOIN `team` t ON t.id=o.TeamId WHERE k.id = '"+obj.insertId+"'";
                                userTypes = '2';
                               
                            }else if(data.ObjectiveType == '3'){
                                qs = "SELECT o.id,o.ObjectiveName,k.id as KeyId, k.KeyResultName,o.MemberId,u.UserName FROM `objectives` o INNER JOIN `keyresults` k ON k.ObjectiveId = o.id INNER JOIN `users` u ON u.id=o.MemberIdWHERE k.id = '"+obj.insertId+"'";
                                userTypes = '3';
                                
                            }    
                            //console.log('ress',qs);
                            db.query(qs, function(e, resu, fields) {
                                if (e) {
                                    return callback({
                                        "statuscode": "203",
                                        "status": "error",
                                        "msg": 'something went wrong',
                                        "error": e
                                    })
                                } else {
                                    //console.log('resu',resu);
                                    if(data.ObjectiveType == '1'){
                                        userIds = resu[0].CompanyId;
                                    }else if(data.ObjectiveType == '2'){
                                        userIds = resu[0].TeamId;
                                    }else if(data.ObjectiveType == '3'){
                                        userIds = resu[0].MemberId;
                                    }

                                    q = "UPDATE `objectives` SET AssignedToOkr = '"+obj.insertId+"', AssignedToOkrUserType = '"+userTypes+"', AssignToOkrUserId = '"+userIds+"' WHERE id='" + data.AssignedByObj + "' ";
                              
                                    //console.log(q);
                                    db.query(q, function(e, result, fields) {
                                        if (e) {
                                            return callback({
                                                "statuscode": "203",
                                                "status": "error",
                                                "msg": 'something went wrong',
                                                "error": e
                                            })
                                        } else {
                                            return callback({
                                                "statuscode": "200",
                                                "status": "success",
                                                "msg": 'Objective assigned successfully',
                                                "data": result
                                            })
                                        }
                                    })
                                    
                                }
                            })

                            
                        }
                })


              
                
            }
    })
}

module.exports.addOKR = function(data, uData, callback) {
    //console.log(data);
    q = "INSERT INTO `keyresults` (`KeyResultName`, `ObjectiveId`, `Status`, `ProgressValue`, `CreatedBy`, `CreatedDate`) VALUES ('"+data.KeyResultName+"','"+data.ObjectiveId+"','"+data.Status+"','"+data.ProgressValue+"','"+data.CreatedBy+"','"+ dateFormat(new Date(), 'yyyy-mm-dd') +"')";
    //console.log(q);
    db.query(q, function(e, teams, fields) {
            if (e) {
                return callback({
                    "statuscode": "203",
                    "status": "error",
                    "msg": 'something went wrong',
                    "error": e
                })
            } else {


                /* For Weekly-checkins Start */
                    var comId,TeaId,MemId;
                    qk = "SELECT * FROM `objectives` WHERE id = '"+data.ObjectiveId+"' AND IsActive = '1'";
                    //console.log('qkqk',qk);
                    db.query(qk, function(e, objs, fields) {
                        if (e) {
                            return callback({
                                "statuscode": "203",
                                "status": "error",
                                "msg": 'something went wrong',
                                "error": e
                            })
                        } else {
                            if(objs.length > 0){
                                //console.log(objs);
                                comId =  objs[0].CompanyId;
                                TeaId =  objs[0].TeamId;
                                MemId =  objs[0].MemberId;

                                qx = "INSERT INTO `weekly_checkins` (`CompanyId`,`TeamId`,`MemberId`,`ObjectiveId`, `ProgressValue`, `CreatedBy`,`CreatedDate`) VALUES ('"+comId+"','"+TeaId+"','"+MemId+"','"+data.ObjectiveId+"','"+data.newValue+"','"+data.CreatedBy+"','"+ dateFormat(new Date(), 'yyyy-mm-dd') +"')";
                                //console.log('aqx',qx);
                                db.query(qx, function(e, fin, fields) {
                                    if (e) {
                                        return callback({
                                            "statuscode": "203",
                                            "status": "error",
                                            "msg": 'something went wrong',
                                            "error": e
                                        })
                                    } else {
                                        
                                    }
                                })
                            }
                            
                        }
                    })

                /* For Weekly-checkins End */

                qs = "SELECT * FROM `keyresults` WHERE ObjectiveId = '"+data.ObjectiveId+"' AND IsActive = '1'";
                db.query(qs, function(e, output, fields) {
                    if (e) {
                        return callback({
                            "statuscode": "203",
                            "status": "error",
                            "msg": 'something went wrong',
                            "error": e
                        })
                    } else {
                        
                        
                        if(output.length > 1){


                            var totalWeightage = 0;
                            
                            var weighatgePerOkr = parseFloat(100/output.length);
                            for(i=0;i<output.length;i++){
                                qw = "UPDATE `keyresults` SET Weightage = '"+weighatgePerOkr+"' WHERE id='" + output[i].id + "' AND IsActive = '1'";
                                db.query(qw, function(e, outputs, fields) {
                                    if (e) {
                                        return callback({
                                            "statuscode": "203",
                                            "status": "error",
                                            "msg": 'something went wrong',
                                            "error": e
                                        })
                                    }else{

                                        
                                    }
                                })
                            }

                            qs = "SELECT * FROM `keyresults` WHERE ObjectiveId = '"+data.ObjectiveId+"' AND IsActive = '1'";
                            db.query(qs, function(e, output, fields) {
                            if (e) {
                                return callback({
                                    "statuscode": "203",
                                    "status": "error",
                                    "msg": 'something went wrong',
                                    "error": e
                                })
                            } else {
                                //console.log(output);
                                //console.log(output.length);
                                var OKRFinalValue = 0;
                                for(i=0;i<output.length;i++){
                                    OKRFinalValue += parseFloat((output[i].ProgressValue/output[i].TargetValue)*output[i].Weightage);
                                    //console.log(OKRFinalValue);
                                }
                                if(OKRFinalValue > 100){
                                    OKRFinalValue = 100;
                                }
                                //console.log(OKRFinalValue);
                                qz = "UPDATE `objectives` SET ProgressValue = '" + Math.round(OKRFinalValue) + "' WHERE id='" + data.ObjectiveId + "' AND IsActive = '1'";
                                //console.log(qz);
                                db.query(qz, function(e, res, fields) {
                                    if (e) {
                                        return callback({
                                            "statuscode": "203",
                                            "status": "error",
                                            "msg": 'something went wrong',
                                            "error": e
                                        })
                                    } else {
                                        return callback({
                                            "statuscode": "200",
                                            "status": "success",
                                            "msg": 'Key result created successfully'
                                        })

                                    }
                                })
                            }
                            })
                            


                        }else{
                            return callback({
                                "statuscode": "200",
                                "status": "success",
                                "msg": 'Key result created successfully'
                            })
                        }

                        
                        
                        /*if(output.length > 1){
                            var leftWeightage = 100 - parseFloat(data.Weightage);
                            var leftOkr = output.length - 1;
                            var weighatgePerOkr = parseFloat(leftWeightage/leftOkr);
                            for(i=0;i<output.length;i++){
                                if(data.okrId != output[i].id){
                                    qw = "UPDATE `keyresults` SET Weightage = '"+weighatgePerOkr+"' WHERE id='" + output[i].id + "' AND IsActive = '1'";
                                    db.query(qw, function(e, outputs, fields) {
                                        if (e) {
                                            return callback({
                                                "statuscode": "203",
                                                "status": "error",
                                                "msg": 'something went wrong',
                                                "error": e
                                            })
                                        }else{

                                            
                                        }
                                    })
                                }
                            }
                            return callback({
                                "statuscode": "200",
                                "status": "success",
                                "msg": 'Key result created successfully'
                            }) 
                        }else{
                            qp = "SELECT * FROM `keyresults` WHERE ObjectiveId = '"+data.ObjectiveId+"' AND IsActive = '1'";
                            db.query(qp, function(e, keys, fields) {
                                if (e) {
                                    return callback({
                                        "statuscode": "203",
                                        "status": "error",
                                        "msg": 'something went wrong',
                                        "error": e
                                    })
                                }else{
                                    var OKRFinalValue = 0;
                                    for(i=0;i<keys.length;i++){
                                        OKRFinalValue += parseFloat((keys[i].ProgressValue/keys[i].TargetValue)*keys[i].Weightage);
                                    }

                                    qv = "UPDATE `objectives` SET ProgressValue = '"+Math.round(OKRFinalValue)+"' WHERE id='" + data.objId + "' ";
                                    db.query(qv, function(e, objs, fields) {
                                        if (e) {
                                            return callback({
                                                "statuscode": "203",
                                                "status": "error",
                                                "msg": 'something went wrong',
                                                "error": e
                                            })
                                        }else{
                                            return callback({
                                                "statuscode": "200",
                                                "status": "success",
                                                "msg": 'Weightage Updated',
                                                "data": objs
                                            })
                                        }
                                    })
                                }
                            })
                        }*/
                    }
                })

                
            }
    })
}


module.exports.updateOKR = function(data, uData, callback) {
    //console.log(data);
    q = "UPDATE `keyresults` SET KeyResultName = '" + data.KeyResultName + "' WHERE id='" + data.okrid + "' ";
    //console.log(q);
    db.query(q, function(e, teams, fields) {
            if (e) {
                return callback({
                    "statuscode": "203",
                    "status": "error",
                    "msg": 'something went wrong',
                    "error": e
                })
            } else {
                return callback({
                    "statuscode": "200",
                    "status": "success",
                    "msg": 'Key result updated successfully',
                    "data": teams
                })
            }
    })
}

module.exports.updateOKRProgress = function(data, uData, callback) {
    //console.log(data);
    q = "UPDATE `keyresults` SET ProgressValue = '" + data.newValue + "' WHERE id='" + data.okrId + "' AND IsActive = '1'";
    //console.log(q);
    db.query(q, function(e, teams, fields) {
        if (e) {
            return callback({
                "statuscode": "203",
                "status": "error",
                "msg": 'something went wrong',
                "error": e
            })
        } else {
            qs = "SELECT * FROM `keyresults` WHERE ObjectiveId = '"+data.objId+"' AND IsActive = '1'";
            db.query(qs, function(e, output, fields) {
                if (e) {
                    return callback({
                        "statuscode": "203",
                        "status": "error",
                        "msg": 'something went wrong',
                        "error": e
                    })
                } else {
                    /*var totalOKR = 0;
                    for(i=0;i<output.length;i++){
                        if(output[i].TargetValue < 100){
                            totalOKR += parseFloat((output[i].ProgressValue/output[i].TargetValue)*100);
                        }else{
                            totalOKR += parseFloat(output[i].ProgressValue);
                        }
                    }

                    var objTotal = Math.round(totalOKR/output.length);*/
                    var OKRFinalValue = 0;
                    for(i=0;i<output.length;i++){
                        OKRFinalValue += parseFloat((output[i].ProgressValue/output[i].TargetValue)*output[i].Weightage);
                        //console.log(OKRFinalValue);
                    }
                    if(OKRFinalValue > 100){
                        OKRFinalValue = 100;
                    }
                    qz = "UPDATE `objectives` SET ProgressValue = '" + Math.round(OKRFinalValue) + "' WHERE id='" + data.objId + "' AND IsActive = '1'";
                    db.query(qz, function(e, res, fields) {
                        if (e) {
                            return callback({
                                "statuscode": "203",
                                "status": "error",
                                "msg": 'something went wrong',
                                "error": e
                            })
                        } else {
                            qw = "INSERT INTO `keyresultcheckin` (`KeyResultId`, `ProgressValue`, `CreatedBy`,`CreatedDate`) VALUES ('"+data.okrId+"','"+data.newValue+"','"+data.CreatedBy+"','"+ dateFormat(new Date(), 'yyyy-mm-dd') +"')";
                            db.query(qw, function(e, fin, fields) {
                                if (e) {
                                    return callback({
                                        "statuscode": "203",
                                        "status": "error",
                                        "msg": 'something went wrong',
                                        "error": e
                                    })
                                } else {
                                    return callback({
                                        "statuscode": "200",
                                        "status": "success",
                                        "msg": 'Progress Updated',
                                        "data": fin
                                    })
                                }
                            })
                        }
                    })
                    var comId,TeaId,MemId;
                    qk = "SELECT * FROM `objectives` WHERE id = '"+data.objId+"' AND IsActive = '1'";
                    //console.log(qk);
                    db.query(qk, function(e, objs, fields) {
                        if (e) {
                            return callback({
                                "statuscode": "203",
                                "status": "error",
                                "msg": 'something went wrong',
                                "error": e
                            })
                        } else {
                            if(objs.length > 0){
                                //console.log(objs);
                                comId =  objs[0].CompanyId;
                                TeaId =  objs[0].TeamId;
                                MemId =  objs[0].MemberId;
                                qv = "UPDATE `keyresults` SET ProgressValue = '" + objs[0].ProgressValue + "' WHERE id='" + objs[0].AssignedToOkr + "' AND IsActive = '1'";

                                db.query(qv, function(e, okrs, fields) {
                                    if (e) {
                                        return callback({
                                            "statuscode": "203",
                                            "status": "error",
                                            "msg": 'something went wrong',
                                            "error": e
                                        })
                                    } else {
                                        qx = "INSERT INTO `weekly_checkins` (`CompanyId`,`TeamId`,`MemberId`,`ObjectiveId`, `ProgressValue`, `CreatedBy`,`CreatedDate`) VALUES ('"+comId+"','"+TeaId+"','"+MemId+"','"+data.objId+"','"+data.newValue+"','"+data.CreatedBy+"','"+ dateFormat(new Date(), 'yyyy-mm-dd') +"')";
                                        //console.log('qx',qx);
                                        db.query(qx, function(e, fin, fields) {
                                            if (e) {
                                                return callback({
                                                    "statuscode": "203",
                                                    "status": "error",
                                                    "msg": 'something went wrong',
                                                    "error": e
                                                })
                                            } else {
                                                
                                            }
                                        })
                                    }
                                })

                                if(objs[0].AssignedByOkr != 0){
                                    qv = "UPDATE `keyresults` SET ProgressValue = '" + objs[0].ProgressValue + "' WHERE id='" + objs[0].AssignedByOkr + "' AND IsActive = '1'";

                                    db.query(qv, function(e, okrs, fields) {
                                        if (e) {
                                            return callback({
                                                "statuscode": "203",
                                                "status": "error",
                                                "msg": 'something went wrong',
                                                "error": e
                                            })
                                        } else {

                                        }
                                    })
                                        //output
                                    qvs = "SELECT * FROM `keyresults` WHERE id = '"+objs[0].AssignedByOkr+"' AND IsActive = '1'";

                                    db.query(qvs, function(e, okrssig, fields) {
                                        if (e) {
                                            return callback({
                                                "statuscode": "203",
                                                "status": "error",
                                                "msg": 'something went wrong',
                                                "error": e
                                            })
                                        } else {
                                            //console.log(okrssig);
                                            qvss = "SELECT * FROM `keyresults` WHERE ObjectiveId = '"+okrssig[0].ObjectiveId+"' AND IsActive = '1'";
                                            db.query(qvss, function(e, okrssigwe, fields) {
                                                if (e) {
                                                    return callback({
                                                        "statuscode": "203",
                                                        "status": "error",
                                                        "msg": 'something went wrong',
                                                        "error": e
                                                    })
                                                } else {
                                                    //console.log(okrssigwe);
                                                    var OKRFinalValue = 0;
                                                    for(i=0;i<okrssigwe.length;i++){
                                                        OKRFinalValue += parseFloat((okrssigwe[i].ProgressValue/okrssigwe[i].TargetValue)*okrssigwe[i].Weightage);
                                                        //console.log(OKRFinalValue);
                                                    }
                                                    if(OKRFinalValue > 100){
                                                        OKRFinalValue = 100;
                                                    }
                                                    qzz = "UPDATE `objectives` SET ProgressValue = '" + Math.round(OKRFinalValue) + "' WHERE id='" + okrssig[0].ObjectiveId + "' AND IsActive = '1'";
                                                    db.query(qzz, function(e, res, fields) {
                                                        if (e) {
                                                            return callback({
                                                                "statuscode": "203",
                                                                "status": "error",
                                                                "msg": 'something went wrong',
                                                                "error": e
                                                            })
                                                        } else {
                                                        }
                                                    })

                                                }
                                            })
                                        }
                                    })

                                }    


                            }
                            
                        }
                    })

                    

                }
            })

            
        }
    })
}

module.exports.deleteOKR = function(data, uData, callback) {
    //console.log(data);
    var cuObj;
    qvs = "SELECT * FROM `keyresults` WHERE id = '"+data.okrid+"' AND IsActive = '1'";
    //console.log(qvs);
    db.query(qvs, function(e, keylist, fields) {
        if (e) {
            return callback({
                "statuscode": "203",
                "status": "error",
                "msg": 'something went wrong',
                "error": e
            })
        } else {
            //console.log(keylist);
            cuObj = keylist[0].ObjectiveId;
            
            q = "UPDATE `keyresults` SET IsActive = 0 WHERE id='" + data.okrid + "' ";
            //console.log(q);
            db.query(q, function(e, teams, fields) {
                if (e) {
                    return callback({
                        "statuscode": "203",
                        "status": "error",
                        "msg": 'something went wrong',
                        "error": e
                    })
                } else { 
                    qvss = "SELECT * FROM `keyresults` WHERE ObjectiveId = '"+cuObj+"' AND IsActive = '1'";
                    db.query(qvss, function(e, okrssigwe, fields) {
                        if (e) {
                            return callback({
                                "statuscode": "203",
                                "status": "error",
                                "msg": 'something went wrong',
                                "error": e
                            })
                        } else {
                            var totalWeightage = 0;
                            
                            var weighatgePerOkr = parseFloat(100/okrssigwe.length);
                            for(i=0;i<okrssigwe.length;i++){
                                qw = "UPDATE `keyresults` SET Weightage = '"+weighatgePerOkr+"' WHERE id='" + okrssigwe[i].id + "' AND IsActive = '1'";
                                db.query(qw, function(e, outputs, fields) {
                                    if (e) {
                                        return callback({
                                            "statuscode": "203",
                                            "status": "error",
                                            "msg": 'something went wrong',
                                            "error": e
                                        })
                                    }else{

                                        
                                    }
                                })
                            }

                            qvss = "SELECT * FROM `keyresults` WHERE ObjectiveId = '"+cuObj+"' AND IsActive = '1'";
                            db.query(qvss, function(e, okrssigwe, fields) {
                                if (e) {
                                    return callback({
                                        "statuscode": "203",
                                        "status": "error",
                                        "msg": 'something went wrong',
                                        "error": e
                                    })
                                } else {
                                    //console.log(okrssigwe);
                                    //console.log(okrssigwe.length);
                                    var OKRFinalValue = 0;
                                    for(i=0;i<okrssigwe.length;i++){
                                        OKRFinalValue += parseFloat((okrssigwe[i].ProgressValue/okrssigwe[i].TargetValue)*okrssigwe[i].Weightage);
                                        //console.log(OKRFinalValue);
                                    }
                                    if(OKRFinalValue > 100){
                                        OKRFinalValue = 100;
                                    }
                                    qzz = "UPDATE `objectives` SET ProgressValue = '" + Math.round(OKRFinalValue) + "' WHERE id='" + cuObj + "' AND IsActive = '1'";
                                    db.query(qzz, function(e, res, fields) {
                                        if (e) {
                                            return callback({
                                                "statuscode": "203",
                                                "status": "error",
                                                "msg": 'something went wrong',
                                                "error": e
                                            })
                                        } else {
                                            return callback({
                                                "statuscode": "200",
                                                "status": "success",
                                                "msg": 'Key result deleted successfully',
                                                "data": res
                                            })
                                        }
                                    })

                                }

                            })

                            

                        }
                    })
                    
                }
            })
        }
    })

    
}

module.exports.addComment = function(data, uData, callback) {
    //console.log(data,uData);
    q = "INSERT INTO `comments` (`PlanId`, `ObjectiveId`, `OkrId`, `Message`, `PostedBy`, `Mentioned`, `CreatedAt`) VALUES ('"+data.PlanId+"','"+data.ObjectiveId+"','"+data.OkrId+"','"+data.Message+"','"+uData.id+"','','"+ dateFormat(new Date(), 'yyyy-mm-dd') +"')";
    //console.log(q);
    db.query(q, function(e, teams, fields) {
            if (e) {
                return callback({
                    "statuscode": "203",
                    "status": "error",
                    "msg": 'something went wrong',
                    "error": e
                })
            } else {
                return callback({
                    "statuscode": "200",
                    "status": "success",
                    "msg": 'Comment added successfully',
                    "data": teams
                })
            }
    })
}

module.exports.updateComment = function(data, uData, callback) {
    //console.log(data,uData);
    if(data.ObjectiveId != 0){
        q = "UPDATE `comments` SET Message = '" + data.Message + "' WHERE id='" + data.ObjectiveId + "' ";
    }else if(data.OkrId != 0){
        q = "UPDATE `comments` SET Message = '" + data.Message + "' WHERE id='" + data.OkrId + "' ";
    }else if(data.PlanId != 0){
        q = "UPDATE `comments` SET Message = '" + data.Message + "' WHERE id='" + data.PlanId + "' ";
    }

    db.query(q, function(e, teams, fields) {
            if (e) {
                return callback({
                    "statuscode": "203",
                    "status": "error",
                    "msg": 'something went wrong',
                    "error": e
                })
            } else {
                return callback({
                    "statuscode": "200",
                    "status": "success",
                    "msg": 'Comment updated successfully',
                    "data": teams
                })
            }
    })
}


module.exports.deleteComment = function(data, uData, callback) {
    //console.log(data,uData);
    if(data.ObjectiveId != 0){
        q = "UPDATE `comments` SET Status = 0 WHERE id='" + data.ObjectiveId + "' ";
    }else if(data.OkrId != 0){
        q = "UPDATE `comments` SET Status = 0 WHERE id='" + data.OkrId + "' ";
    }else if(data.PlanId != 0){
        q = "UPDATE `comments` SET Status = 0 WHERE id='" + data.PlanId + "' ";
    }

    db.query(q, function(e, teams, fields) {
            if (e) {
                return callback({
                    "statuscode": "203",
                    "status": "error",
                    "msg": 'something went wrong',
                    "error": e
                })
            } else {
                return callback({
                    "statuscode": "200",
                    "status": "success",
                    "msg": 'Comment deleted successfully',
                    "data": teams
                })
            }
    })
}

module.exports.getCompanyPlans = function(data, uData, callback) {

    q = "SELECT *, 0 AS totalLinked, (SELECT DATEDIFF(DueDate,CURDATE())) AS Dues FROM plans WHERE CompanyId='" + uData.CompanyId + "' AND TeamId='0' AND MemberId='0' AND DATE(CreatedDate)>=DATE('" + data.FromDate + "') AND DATE(CreatedDate)<=DATE('" + data.ToDate + "') AND IsActive = '1'";
    //console.log(q);
    db.query(q, function(e, plans, fields) {
        if (e) {
            return callback({
                "statuscode": "203",
                "status": "error",
                "msg": 'something went wrong',
                "error": e
            })
        } else {
            //console.log(plans);
            if(plans.length != 0){

            
            var plan_ids = [];
            plans.forEach((p, i) => {
                plan_ids.push(p.id);
            });

            var qqqq = [];
            if (plan_ids.length > 0) {
                qqqq.push("c.PlanId IN (" + plan_ids.join(',') + ")");
            } else {
                qqqq.push("c.PlanId IN (0)");
            }
           

            q = "SELECT c.*,tm.UserName FROM comments c LEFT JOIN users tm ON tm.id=c.PostedBy WHERE c.Status ='1' AND " + qqqq.join(' OR ');
            //console.log(q);
            db.query(q, function(err, comments, fields) {
                if (err) {
                    return callback({ "statuscode": "203", "status": "error", "msg": "something went wrong", "error": err });
                } else {
                    //console.log('comments',comments);

                    var plns = plans;
                    plans.forEach((p, i) => {
                        plns[i]['comments'] = [];
                        comments.forEach((cmt) => {
                            if (p.id == cmt.PlanId) {
                                plns[i]['comments'].push(cmt);
                            }
                        })
                    });
                    qv = "SELECT COUNT(PlanId) AS totalLinked, PlanId FROM `linked_plans` WHERE PlanId IN ("+plan_ids+") GROUP BY PlanId";
                        //console.log(qv);
                        db.query(qv, function(err, links, fields) {
                            if (err) {

                                   } else {
                                var d;
                                    d = { done: [], plans: [], problems: [] };
                                    plans.forEach((p, p_i) => {
                                        if (p.Type == 0) {
                                            
                                            for(i=0;i<links.length;i++){
                                                if(p.id == links[i].PlanId){
                                                    p.totalLinked = links[i].totalLinked;
                                                }
                                            }
                                            d.plans.push(p);
                                            //console.log(d.plans);
                                        } else if (p.Type == 1) {
                                            for(i=0;i<links.length;i++){
                                                if(p.id == links[i].PlanId){
                                                    p.totalLinked = links[i].totalLinked;
                                                }
                                            }
                                            d.done.push(p);
                                        } else {
                                            for(i=0;i<links.length;i++){
                                                if(p.id == links[i].PlanId){
                                                    p.totalLinked = links[i].totalLinked;
                                                }
                                            }
                                            d.problems.push(p);
                                        }
                                    })
                                   return callback({ "statuscode": "200", "status": "success", "data": d });
                            }
                        });
                }

            })
            }else{
                return callback({ "statuscode": "203", "status": "success", "data": 'NULL' });
            }
        }
    });
};

module.exports.getTeamPlans = function(data, uData, callback) {

    q = "SELECT *, 0 AS totalLinked, (SELECT DATEDIFF(DueDate,CURDATE())) AS Dues FROM plans WHERE CompanyId='" + uData.CompanyId + "' AND TeamId='"+ data.TeamId +"' AND MemberId='0' AND DATE(CreatedDate)>=DATE('" + data.FromDate + "') AND DATE(CreatedDate)<=DATE('" + data.ToDate + "') AND IsActive = '1'";
    
    db.query(q, function(e, plans, fields) {
        if (e) {
            return callback({
                "statuscode": "203",
                "status": "error",
                "msg": 'something went wrong',
                "error": e
            })
        } else {
            if(plans.length != 0){
            var plan_ids = [];
            plans.forEach((p, i) => {
                plan_ids.push(p.id);
            });

            //console.log(plan_ids);
            var qqqq = [];
            if (plan_ids.length > 0) {
                qqqq.push("c.PlanId IN (" + plan_ids.join(',') + ")");
            } else {
                qqqq.push("c.PlanId IN (0)");
            }

            q = "SELECT c.*,tm.UserName FROM comments c LEFT JOIN users tm ON tm.id=c.PostedBy WHERE c.Status ='1' AND " + qqqq.join(' OR ');
            //console.log(q);
            db.query(q, function(err, comments, fields) {
                if (err) {
                    return callback({ "statuscode": "203", "status": "error", "msg": "something went wrong", "error": err });
                } else {
                    //console.log('comments',comments);
                    var plns = plans;
                    plans.forEach((p, i) => {
                        plns[i]['comments'] = [];
                        comments.forEach((cmt) => {
                            if (p.id == cmt.PlanId) {
                                plns[i]['comments'].push(cmt);
                            }
                        })
                    });
                    qv = "SELECT COUNT(PlanId) AS totalLinked, PlanId FROM `linked_plans` WHERE PlanId IN ("+plan_ids+") GROUP BY PlanId";
                        //console.log(qv);
                        db.query(qv, function(err, links, fields) {
                            if (err) {

                                } else {
                                var d;
                                d = { done: [], plans: [], problems: [] };
                                plans.forEach((p, p_i) => {
                                    if (p.Type == 0) {
                                        for(i=0;i<links.length;i++){
                                            if(p.id == links[i].PlanId){
                                                p.totalLinked = links[i].totalLinked;
                                            }
                                        }
                                        d.plans.push(p);
                                    } else if (p.Type == 1) {
                                        for(i=0;i<links.length;i++){
                                            if(p.id == links[i].PlanId){
                                                p.totalLinked = links[i].totalLinked;
                                            }
                                        }
                                        d.done.push(p);
                                    } else {
                                        for(i=0;i<links.length;i++){
                                            if(p.id == links[i].PlanId){
                                                p.totalLinked = links[i].totalLinked;
                                            }
                                        }
                                        d.problems.push(p);
                                    }
                                })
                                //console.log(d);

                               return callback({ "statuscode": "200", "status": "success", "data": d });
                               }
                        });
                }

            })
            }else{
                return callback({ "statuscode": "203", "status": "success", "data": 'NULL' });
            }
        }
    });
};

module.exports.getMemberPlans = function(data, uData, callback) {

    q = "SELECT *, 0 AS totalLinked, (SELECT DATEDIFF(DueDate,CURDATE())) AS Dues FROM plans WHERE CompanyId='" + uData.CompanyId + "' AND TeamId='"+ data.TeamId +"' AND MemberId='"+ data.MemberId +"' AND DATE(CreatedDate)>=DATE('" + data.FromDate + "') AND DATE(CreatedDate)<=DATE('" + data.ToDate + "') AND IsActive = '1'";
    //console.log(q);
    db.query(q, function(e, plans, fields) {
        if (e) {
            return callback({
                "statuscode": "203",
                "status": "error",
                "msg": 'something went wrong',
                "error": e
            })
        } else {
            if(plans.length != 0){
            var plan_ids = [];
            plans.forEach((p, i) => {
                plan_ids.push(p.id);
            });

            //console.log(plan_ids);
            var qqqq = [];
            if (plan_ids.length > 0) {
                qqqq.push("c.PlanId IN (" + plan_ids.join(',') + ")");
            } else {
                qqqq.push("c.PlanId IN (0)");
            }

            q = "SELECT c.*,tm.UserName FROM comments c LEFT JOIN users tm ON tm.id=c.PostedBy WHERE c.Status ='1' AND " + qqqq.join(' OR ');
            //console.log(q);
            db.query(q, function(err, comments, fields) {
                if (err) {
                    return callback({ "statuscode": "203", "status": "error", "msg": "something went wrong", "error": err });
                } else {
                    //console.log('comments',comments);
                    var plns = plans;
                    plans.forEach((p, i) => {
                        plns[i]['comments'] = [];
                        comments.forEach((cmt) => {
                            if (p.id == cmt.PlanId) {
                                plns[i]['comments'].push(cmt);
                            }
                        })
                    });

                    qv = "SELECT COUNT(PlanId) AS totalLinked, PlanId FROM `linked_plans` WHERE PlanId IN ("+plan_ids+") GROUP BY PlanId";
                        //console.log(qv);
                        db.query(qv, function(err, links, fields) {
                            if (err) {
                                    return callback({ "statuscode": "203", "status": "error", "msg": "something went wrong", "error": err });
                                } else {
                                        var d;
                                        d = { done: [], plans: [], problems: [] };
                                        plans.forEach((p, p_i) => {
                                            if (p.Type == 0) {
                                                for(i=0;i<links.length;i++){
                                                    if(p.id == links[i].PlanId){
                                                        p.totalLinked = links[i].totalLinked;
                                                    }
                                                }
                                                d.plans.push(p);
                                            } else if (p.Type == 1) {
                                                for(i=0;i<links.length;i++){
                                                    if(p.id == links[i].PlanId){
                                                        p.totalLinked = links[i].totalLinked;
                                                    }
                                                }
                                                d.done.push(p);
                                            } else {
                                                for(i=0;i<links.length;i++){
                                                    if(p.id == links[i].PlanId){
                                                        p.totalLinked = links[i].totalLinked;
                                                    }
                                                }
                                                d.problems.push(p);
                                            }
                                        })
                                        //console.log(d);

                                       return callback({ "statuscode": "200", "status": "success", "data": d });
                                   }
                        });
                }

            })
            }else{
                return callback({ "statuscode": "203", "status": "success", "data": 'NULL' });
            }
        }
    });
};


module.exports.addPlan = function(data, uData, callback) {
    //console.log(data.Title);
    if(data.FromObj == 'Yes'){
        q = "INSERT INTO `plans` (`LinkedToObj`, `Title`, `CompanyId`, `TeamId`, `MemberId`,`DueDate`, `Type`, `CreatedBy`, `CreatedDate`) VALUES ('"+data.LinkedToObj+"','"+data.Title+"','"+data.CompanyId+"','"+data.TeamId+"','"+data.MemberId+"','"+data.DueDate+"','"+data.Type+"','"+data.CreatedBy+"','"+ dateFormat(new Date(), 'yyyy-mm-dd') +"')";
    }else{
        q = "INSERT INTO `plans` (`Title`, `CompanyId`, `TeamId`, `MemberId`,`DueDate`, `Type`, `CreatedBy`, `CreatedDate`) VALUES ('"+data.Title+"','"+data.CompanyId+"','"+data.TeamId+"','"+data.MemberId+"','"+data.DueDate+"','"+data.Type+"','"+data.CreatedBy+"','"+ dateFormat(new Date(), 'yyyy-mm-dd') +"')";
    }

    db.query(q, function(e, plans, fields) {
            if (e) {
                return callback({
                    "statuscode": "203",
                    "status": "error",
                    "msg": 'something went wrong',
                    "error": e
                })
            } else {
                return callback({
                    "statuscode": "200",
                    "status": "success",
                    "msg": 'New item added',
                    "data": plans
                })
            }
    })
}

module.exports.addPlanFromObjective = function(data, uData, callback) {
    
    if(data.FromObj == 'Yes'){
        q = "INSERT INTO `plans` (`LinkedToObj`, `Title`, `CompanyId`, `TeamId`, `MemberId`,`DueDate`, `Type`, `CreatedBy`, `CreatedDate`) VALUES ('"+data.LinkedToObj+"','"+data.Title+"','"+data.CompanyId+"','"+data.TeamId+"','"+data.MemberId+"','"+data.DueDate+"','"+data.Type+"','"+data.CreatedBy+"','"+ dateFormat(new Date(), 'yyyy-mm-dd') +"')";
    }else{
        q = "INSERT INTO `plans` (`Title`, `CompanyId`, `TeamId`, `MemberId`,`DueDate`, `Type`, `CreatedBy`, `CreatedDate`) VALUES ('"+data.Title+"','"+data.CompanyId+"','"+data.TeamId+"','"+data.MemberId+"','"+data.DueDate+"','"+data.Type+"','"+data.CreatedBy+"','"+ dateFormat(new Date(), 'yyyy-mm-dd') +"')";
    }

    db.query(q, function(e, plans, fields) {
            if (e) {
                return callback({
                    "statuscode": "203",
                    "status": "error",
                    "msg": 'something went wrong',
                    "error": e
                })
            } else {

                /* Linking Objective */

                var u_l = [],
                    l_k = [];

                l_k.push({ ObjectiveId: data.LinkedToObj, PlanId: plans.insertId });

                async function exc() {

                    var obj = await exeQuery({ q: "DELETE FROM linked_plans WHERE ObjectiveId IN (" + u_l.join(',') + ") AND PlanId='" + plans.insertId + "'" });

                    for (var i = 0; i < l_k.length; i++) {
                        // l_k.forEach(function(k, index) {
                        var obj = await exeQuery({ q: "INSERT INTO linked_plans SET ?", d: l_k[i] });
                    }

                    //return callback({ "statuscode": "200", "status": "success", "msg": "Updated successfully" });

                }
                exc();

                return callback({
                    "statuscode": "200",
                    "status": "success",
                    "msg": 'New item added',
                    "data": plans
                })
            }
    })
}

module.exports.updatePlan = function(data, uData, callback) {
    if(data.updateType == 'drag'){
        q = "UPDATE `plans` SET Type = '" + data.Type + "', UpdatedBy = '" + data.UpdatedBy + "' WHERE id='" + data.id + "' ";
    }else if(data.updateType == 'updateDueDate'){
        q = "UPDATE `plans` SET DueDate = '" + data.DueDate + "' WHERE id='" + data.id + "' ";  
    }else{
        q = "UPDATE `plans` SET Title = '" + data.Title + "', UpdatedBy = '" + data.UpdatedBy + "' WHERE id='" + data.id + "' ";  
    }
    
    //console.log(data,q);
    db.query(q, function(e, plans, fields) {
            if (e) {
                return callback({
                    "statuscode": "203",
                    "status": "error",
                    "msg": 'something went wrong',
                    "error": e
                })
            } else {
                return callback({
                    "statuscode": "200",
                    "status": "success",
                    "msg": 'Item updated',
                    "data": plans
                })
            }
    })
}

module.exports.deletePlan = function(data, uData, callback) {
    q = "UPDATE `plans` SET IsActive = '0' WHERE id='" + data.id + "' ";
    //console.log(q);
    db.query(q, function(e, plans, fields) {
            if (e) {
                return callback({
                    "statuscode": "203",
                    "status": "error",
                    "msg": 'something went wrong',
                    "error": e
                })
            } else {
                return callback({
                    "statuscode": "200",
                    "status": "success",
                    "msg": 'Item deleted',
                    "data": plans
                })
            }
    })
}

module.exports.getObjectivesData = (data, uData, callback) => {
    var list = {};
    list['personal_objs'] = [];
    list['company_objs'] = [];
    list['team_objs'] = [];

    var q = "SELECT ob.id,ob.ObjectiveName,ob.ProgressValue,ob.CompanyId,ob.TeamId,ob.MemberId,ob.ObjectiveType,us.UserName,tm.TeamName FROM `objectives` ob INNER JOIN `users` us ON us.id = ob.MemberId INNER JOIN `team` tm ON tm.id = ob.TeamId WHERE ob.CompanyId = '"+ uData.CompanyId + "' AND ob.ObjectiveType = '3' AND ob.IsActive = '1'";
    db.query(q, function(e, objList, fields) {
        if (e) {
            return callback({
                "statuscode": "203",
                "status": "error",
                "msg": 'something went wrong',
                "error": e
            })
        } else {
            list['personal_objs'].push(objList);
            var q = "SELECT ob.id,ob.ObjectiveName,ob.ProgressValue,ob.CompanyId,ob.TeamId,ob.MemberId,ob.ObjectiveType,cm.Name AS CompanyName FROM `objectives` ob INNER JOIN `company` cm ON cm.id = ob.CompanyId WHERE ob.CompanyId = '"+ uData.CompanyId + "' AND ob.ObjectiveType = '1' AND ob.IsActive = '1'";
            db.query(q, function(e, objList, fields) {
                if (e) {
                    return callback({
                        "statuscode": "203",
                        "status": "error",
                        "msg": 'something went wrong',
                        "error": e
                    })
                } else {
                    list['company_objs'].push(objList);
                    var q = "SELECT ob.id,ob.ObjectiveName,ob.ProgressValue,ob.CompanyId,ob.TeamId,ob.MemberId,ob.ObjectiveType,tm.TeamName FROM `objectives` ob INNER JOIN `team` tm ON tm.id = ob.TeamId WHERE ob.CompanyId = '"+ uData.CompanyId + "' AND ob.ObjectiveType = '2' AND ob.IsActive = '1'";
                    db.query(q, function(e, objList, fields) {
                        if (e) {
                            return callback({
                                "statuscode": "203",
                                "status": "error",
                                "msg": 'something went wrong',
                                "error": e
                            })
                        } else {
                            list['team_objs'].push(objList);
                            return callback({
                                "statuscode": "200",
                                "status": "success",
                                "msg": 'Personal list',
                                "data" : list
                            })
                        }
                    })

                }
            })

        }
    })
}

module.exports.assignPlansToObjective = function(data, uData, callback) {
    //console.log(data);
    q ="UPDATE `plans` SET LinkedToObj = '"+data.LinkedToObj+"' WHERE id='" + data.PlanId + "' ";

    db.query(q, function(e, objList, fields) {
        if (e) {
            return callback({
                "statuscode": "203",
                "status": "error",
                "msg": 'something went wrong',
                "error": e
            })
        } else {
            return callback({
                "statuscode": "200",
                "status": "success",
                "msg": 'Item linked',
                "data" : objList
            })
        }
    })
}


module.exports.linkPlan = (data, user, callback) => {
    //console.log(data);
    var u_l = [],
        l_k = [];
    var ids = Object.keys(data.ObjectiveId);
    //console.log(ids);
    ids.forEach(function(k, index) {
        //console.log(k);
        if (data.ObjectiveId[k] == 0) {
            u_l.push("'" + k + "'");
        } else {
            l_k.push({ ObjectiveId: k, PlanId: data.PlanId });
            q = "UPDATE plans SET LinkedToObj='" + k + "' WHERE id ='"+data.PlanId+"'";
            //console.log(q);
            db.query(q, data, function(err, result, fields) {
                /*if (err) return callback({ "statuscode": "203", "status": "error", "msg": "Error occurered while updating plans", "error": err });
                else {
                    if (result.affectedRows > 0)
                        return callback({ "statuscode": "200", "status": "success", "msg": "plans updated successfully" });
                    else return callback({ "statuscode": "201", "status": "failed", "msg": "plans not found" });
                }*/
            });
        }
    });


    async function exc() {
        // console.log("DELETE FROM linked_plans WHERE ObjectiveId IN (" + u_l.join(',') + ") AND PlanId='" + data.PlanId + "'" );
        var obj = await exeQuery({ q: "DELETE FROM linked_plans WHERE ObjectiveId IN (" + u_l.join(',') + ") AND PlanId='" + data.PlanId + "'" });

        // q = "SELECT * FROM linked_plans WHERE PlanId='" + data.PlanId + "' AND ObjectiveId='" + data.ObjectiveId + "'";
        // db.query(q, function(err, result, fields) {
        // if (err) {
        //     return callback({ "statuscode": "203", "status": "error", "msg": "something went wrong", "error": err });
        // } else {
        //     if (result.length < 1) {
        for (var i = 0; i < l_k.length; i++) {
            // l_k.forEach(function(k, index) {
            var obj = await exeQuery({ q: "INSERT INTO linked_plans SET ?", d: l_k[i] });
        }

        // q = "INSERT INTO linked_plans SET ?";
        // db.query(q, l_k, function(err, result, fields) {
        //     if (err) {
        //         return callback({ "statuscode": "203", "status": "error", "msg": "something went wrong", "error": err });
        //     } else {
        return callback({ "statuscode": "200", "status": "success", "msg": "Updated successfully" });
        //     }
        // });
        // } else {
        //     return callback({ "statuscode": "200", "status": "warning", "msg": "Plan already linked with this object" });
        // }
        // }
        // });
    }
    exc();

}

function exeQuery(data) {
    //console.log(data);

    return new Promise(resolve => {
        if (data.d == undefined) {
            db.query(data.q, function(err, updateObj, fields) {
                // console.log(err,updateObj);

                resolve(updateObj);
            });
        } else {
            db.query(data.q, data.d, function(err, updateObj, fields) {
                // console.log(err,updateObj);

                resolve(updateObj);
            });
        }

    });
}

module.exports.getMissionVision = function(data, uData, callback) {
    //console.log(data);
    q ="SELECT * FROM `missionvision` WHERE CompanyId='" + data.CompanyId + "' ";

    db.query(q, function(e, missionvision, fields) {
        if (e) {
            return callback({
                "statuscode": "203",
                "status": "error",
                "msg": 'something went wrong',
                "error": e
            })
        } else {
            return callback({
                "statuscode": "200",
                "status": "success",
                "msg": 'success',
                "data" : missionvision
            })
        }
    })
}

module.exports.submitMissionVision = function(data, uData, callback) {
    //console.log(data);
    q ="INSERT INTO `missionvision` (`CompanyId`,`Mission`,`Vision`) VALUES ('"+data.CompanyId+"','"+data.Mission+"','"+data.Vision+"')";

    db.query(q, function(e, missionvision, fields) {
        if (e) {
            return callback({
                "statuscode": "203",
                "status": "error",
                "msg": 'something went wrong',
                "error": e
            })
        } else {
            return callback({
                "statuscode": "200",
                "status": "success",
                "msg": 'success',
                "data" : missionvision
            })
        }
    })
}

module.exports.updateMissionVision = function(data, uData, callback) {
    //console.log(data);
    q = "UPDATE missionvision SET Mission='" + data.Mission + "',Vision='" + data.Vision + "' WHERE CompanyId ='"+data.CompanyId+"'";
    db.query(q, function(e, missionvision, fields) {
        if (e) {
            return callback({
                "statuscode": "203",
                "status": "error",
                "msg": 'something went wrong',
                "error": e
            })
        } else {
            return callback({
                "statuscode": "200",
                "status": "success",
                "msg": 'success',
                "data" : missionvision
            })
        }
    })
}