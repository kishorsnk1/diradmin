var db = require('../config/database');
var config = require('../config/general');
var md5 = require('md5');
var jwt = require('jsonwebtoken');
var common = require('../models/common');
var bcrypt = require('bcryptjs');
var crypto = require('crypto');
var dateFormat = require('dateformat');

module.exports.getLinkDataOKR = (data, uData, callback) => {
   //console.log(data, uData);

    if(data.Type == 'KeyResult'){
       var q ="SELECT us.id,us.UserName,us.CompanyId,mm.TeamId,tm.TeamName FROM members mm INNER JOIN `users` us ON us.id = mm.UserId INNER JOIN `team` tm ON tm.id = mm.TeamId WHERE us.IsActive = 1 AND us.CompanyId = '" + uData.CompanyId + "'";

        db.query(q, function(e, users, fields) {
            if (e) {
                return callback({
                    "statuscode": "203",
                    "status": "error",
                    "msg": 'something went wrong',
                    "error": e
                })
            } else {
                var qq ="SELECT * FROM `team` WHERE IsActive = 1 AND CompanyId = '" + uData.CompanyId + "'";
                db.query(qq, function(e, teams, fields) {
                    if (e) {
                        return callback({
                            "statuscode": "203",
                            "status": "error",
                            "msg": 'something went wrong',
                            "error": e
                        })
                    } else {
                        var list = {};
                        list['persons'] = users;
                        list['team'] = teams;

                        var qqq ="SELECT * FROM `company` WHERE IsActive = 1 AND id = '" + uData.CompanyId + "'";
                        db.query(qqq, function(e, company, fields) {
                            if (e) {
                                return callback({
                                    "statuscode": "203",
                                    "status": "error",
                                    "msg": 'something went wrong',
                                    "error": e
                                })
                            } else {
                                list['company'] = company;
                                return callback({
                                    "statuscode": "200",
                                    "status": "success",
                                    "msg": 'Personal list',
                                    "data" : list
                                })

                            }
                        });
                        
                    }
                })
                
            }
        })
    }

    if(data.Type == 'Objective'){

        var list = {};
        list['personal_objs'] = [];
        list['company_objs'] = [];
        list['team_objs'] = [];

        var q = "SELECT ob.id,ob.ObjectiveName,ob.ProgressValue,ob.CompanyId,ob.TeamId,ob.MemberId,ob.ObjectiveType,us.UserName,tm.TeamName FROM `objectives` ob INNER JOIN `users` us ON us.id = ob.MemberId INNER JOIN `team` tm ON tm.id = ob.TeamId WHERE ob.CompanyId = '"+ uData.CompanyId + "' AND ob.ObjectiveType = '3'";
        db.query(q, function(e, objList, fields) {
            if (e) {
                return callback({
                    "statuscode": "203",
                    "status": "error",
                    "msg": 'something went wrong',
                    "error": e
                })
            } else {
                list['personal_objs'].push(objList);
                var q = "SELECT ob.id,ob.ObjectiveName,ob.ProgressValue,ob.CompanyId,ob.TeamId,ob.MemberId,ob.ObjectiveType,cm.Name AS CompanyName FROM `objectives` ob INNER JOIN `company` cm ON cm.id = ob.CompanyId WHERE ob.CompanyId = '"+ uData.CompanyId + "' AND ob.ObjectiveType = '1'";
                db.query(q, function(e, objList, fields) {
                    if (e) {
                        return callback({
                            "statuscode": "203",
                            "status": "error",
                            "msg": 'something went wrong',
                            "error": e
                        })
                    } else {
                        list['company_objs'].push(objList);
                        var q = "SELECT ob.id,ob.ObjectiveName,ob.ProgressValue,ob.CompanyId,ob.TeamId,ob.MemberId,ob.ObjectiveType,tm.TeamName FROM `objectives` ob INNER JOIN `team` tm ON tm.id = ob.TeamId WHERE ob.CompanyId = '"+ uData.CompanyId + "' AND ob.ObjectiveType = '2'";
                        db.query(q, function(e, objList, fields) {
                            if (e) {
                                return callback({
                                    "statuscode": "203",
                                    "status": "error",
                                    "msg": 'something went wrong',
                                    "error": e
                                })
                            } else {
                                list['team_objs'].push(objList);
                                return callback({
                                    "statuscode": "200",
                                    "status": "success",
                                    "msg": 'Personal list',
                                    "data" : list
                                })
                            }
                        })

                    }
                })

            }
        })

    }

    

}

var getAdmin = (admin_Id, next) => {

    //q = 'SELECT * FROM users WHERE id="' + admin_Id + '"';
    q ='SELECT u.CompanyId,u.CreatedDate,u.Email,u.IsActive,u.JobPosition,u.ModifiedDate,u.Phone,u.UserName,u.UserTypeId,u.id,t.id as TeamId FROM users u INNER JOIN team t ON t.UserId = u.id WHERE u.id="' + admin_Id + '" GROUP BY u.id AND u.IsActive ="1"';
    db.query(q, function(err, admin, fields) {
        if (err) {
            next(err, '');
        } else {
            next(err, admin);
        }
    })
}

var getMembers = (member_ids, next) => {

    q = "SELECT m.UserId as id,m.TeamId,u.UserName FROM members m INNER JOIN users u ON u.id = m.UserId WHERE TeamId IN (" + member_ids + ") AND u.IsActive = '1'";
    db.query(q, function(err, membersList, fields) {
        //console.log('mm', membersList, err)
        if (err) {
            next(err, '');
        } else {
            next(err, membersList);
        }
    })
}

function getOBJS(data, next) {
    q = 'SELECT * FROM objectives WHERE UserId="' + data.userid + '" AND TeamId="' + data.teamid + '" AND MemberId="' + data.memberid + '" WHERE IsActive = 1';
    db.query(q, function(err, objectivesList, fields) {
        if (err) {
            next(err, '');
        } else {
            next(err, objectivesList);
        }
    })
}

function getOKRS(obj_id, next) {

    q = 'SELECT * FROM keyresults WHERE ObjectiveId IN (' + obj_id + ') AND IsActive =1';
    db.query(q, function(err, okrList, fields) {
        if (err) {
            next(err, '');
        } else {
            next(err, okrList);
        }
    })
}

function getComments(obj_id, okr_id, next) {
    // console.log(obj_id, okr_id)
    var c_list = []
    // console.log(c_list.constructor)
    if (obj_id != undefined && obj_id != '') {
        c_list.push('ObjectiveId IN (' + obj_id + ')');
    }
    if (okr_id != undefined && okr_id != '') {
        c_list.push('OkrId IN (' + okr_id + ')');
    }
    q = "SELECT * FROM comments ";
    if (c_list.length > 0) {
        q += "WHERE Status = 1 AND " + c_list.join(" OR ");
        q += " AND Status = 1";
    }
    //console.log('comment',q);
    db.query(q, function(err, CommentList, fields) {
        if (err) {
            next(err, '');
        } else {

            // for()
            // console.log(CommentList[2].PostedBy)

            next(err, CommentList);
        }
    })
}

module.exports.getAllUsers = function(data, uData, callback) {
    //console.log('ddtata',data);
    q = "SELECT * FROM users";
    db.query(q, function(e, users, fields) {
            if (e) {
                return callback({
                    "statuscode": "203",
                    "status": "error",
                    "msg": 'something went wrong',
                    "error": e
                })
            } else {

                return callback(users);
            }
    })         
}

module.exports.getCompanyTotal = function(data, uData, callback) {
    var ObjectivesCount,OKRCount,TeamsCount,MembersCount;
    q = "SELECT COUNT(*) AS ObjectivesCount FROM objectives WHERE CompanyId = '"+uData.CompanyId+"' AND IsActive ='1'";
    db.query(q, function(e, data, fields) {
            if (e) {
                return callback({
                    "statuscode": "203",
                    "status": "error",
                    "msg": 'something went wrong',
                    "error": e
                })
            } else {
                return callback(data);  
            }
    })         
}

module.exports.getTeamTotal = function(data, uData, callback) {
    var ObjectivesCount,OKRCount,TeamsCount,MembersCount;
    q = "SELECT COUNT(*) AS TeamsCount FROM team WHERE CompanyId = '"+uData.CompanyId+"' AND IsActive ='1'";
    db.query(q, function(e, data, fields) {
            if (e) {
                return callback({
                    "statuscode": "203",
                    "status": "error",
                    "msg": 'something went wrong',
                    "error": e
                })
            } else {
                return callback(data);  
            }
    })         
}

module.exports.getMembersTotal = function(data, uData, callback) {
    var ObjectivesCount,OKRCount,TeamsCount,MembersCount;
    q = "SELECT COUNT(*) AS MembersCount FROM users WHERE CompanyId = '"+uData.CompanyId+"' AND IsActive ='1'";
    db.query(q, function(e, data, fields) {
            if (e) {
                return callback({
                    "statuscode": "203",
                    "status": "error",
                    "msg": 'something went wrong',
                    "error": e
                })
            } else {
                return callback(data);  
            }
    })         
}

module.exports.getAllCompanies = function(data, uData, callback) {
    q = "SELECT * FROM users WHERE UserTypeId = '1'";
    db.query(q, function(e, users, fields) {
            if (e) {
                return callback({
                    "statuscode": "203",
                    "status": "error",
                    "msg": 'something went wrong',
                    "error": e
                })
            } else {
                return callback(users);
            }
    })         
}

module.exports.getAllMembers = function(data, uData, callback) {
    q = "SELECT * FROM users WHERE UserTypeId = '3' AND CompanyId = '"+uData.CompanyId+"' AND IsActive!='2'";
    db.query(q, function(e, users, fields) {
            if (e) {
                return callback({
                    "statuscode": "203",
                    "status": "error",
                    "msg": 'something went wrong',
                    "error": e
                })
            } else {
                return callback(users);
            }
    })         
}

module.exports.getAllMentors = function(data, uData, callback) {
    q = "SELECT * FROM users WHERE UserTypeId = '2'";
    db.query(q, function(e, users, fields) {
            if (e) {
                return callback({
                    "statuscode": "203",
                    "status": "error",
                    "msg": 'something went wrong',
                    "error": e
                })
            } else {
                return callback(users);
            }
    })         
}

module.exports.getAllocatedMentors = function(data, uData, callback) {
    q = "SELECT a.CompanyId as id,u.Name as UserName FROM company u INNER JOIN allocatementor a ON a.MentorId = "+data.id+" WHERE u.id = a.CompanyId";
    //console.log(q);
    db.query(q, function(e, users, fields) {
            if (e) {
                return callback({
                    "statuscode": "203",
                    "status": "error",
                    "msg": 'something went wrong',
                    "error": e
                })
            } else {
                return callback(users);
            }
    })         
}

module.exports.updateAllocateMentors = function(data, uData, callback) {
    q = "DELETE FROM allocatementor WHERE MentorId= '" + data[0].MentorId + "'";
    db.query(q, data.data, function(e, result, fields) {
            if (e) {
                return callback({
                    "statuscode": "203",
                    "status": "error",
                    "msg": 'something went wrong',
                    "error": e
                })
            } else {
                var values = [];
                for(var i=0; i<data.length; i++){
                    values.push([CompanyId= data[i].CompanyId, MentorId= data[i].MentorId])
                }
                q = "INSERT INTO `allocateMentor` (`CompanyId`, `MentorId`) VALUES ?";

                db.query(q, [values], function(e, users, fields) {
                        if (e) {
                            return callback({
                                "statuscode": "203",
                                "status": "error",
                                "msg": 'something went wrong',
                                "error": e
                            })
                        } else {
                            return callback({
                                "statuscode": "200",
                                "status": "success",
                                "msg": 'Mentor allocated successfully'
                            })
                        }
                }) 
            }
    })       
}

module.exports.updateCompanyStatus = function(data, uData, callback) {
    q = "UPDATE users SET IsActive='"+ data.IsActive +"' WHERE id='" + data.id + "' ";
    db.query(q, function(e, users, fields) {
            if (e) {
                return callback({
                    "statuscode": "203",
                    "status": "error",
                    "msg": 'something went wrong',
                    "error": e
                })
            } else {
                return callback({
                    "statuscode": "200",
                    "status": "success",
                    "msg": 'Company updated successfully'
                })
            }
    })         
}


module.exports.updateMentorStatus = function(data, uData, callback) {
    q = "UPDATE users SET IsActive='"+ data.IsActive +"' WHERE id='" + data.id + "' ";
    db.query(q, function(e, users, fields) {
            if (e) {
                return callback({
                    "statuscode": "203",
                    "status": "error",
                    "msg": 'something went wrong',
                    "error": e
                })
            } else {
                return callback({
                    "statuscode": "200",
                    "status": "success",
                    "msg": 'Mentor updated successfully'
                })
            }
    })         
}

module.exports.updateMemberStatus = function(data, uData, callback) {
    q = "UPDATE users SET IsActive='"+ data.IsActive +"' WHERE id='" + data.id + "' ";
    db.query(q, function(e, users, fields) {
            if (e) {
                return callback({
                    "statuscode": "203",
                    "status": "error",
                    "msg": 'something went wrong',
                    "error": e
                })
            } else {
                return callback({
                    "statuscode": "200",
                    "status": "success",
                    "msg": 'Member updated successfully'
                })
            }
    })         
}



module.exports.UpdateUserPassword = function(data, uData, callback) {
    //console.log('ddtata',data);
    var pass ='';
    //const salt = bcrypt.genSalt(20);
      pass = crypto.createHash('md5').update(data.Password).digest("hex");
    var q = "UPDATE team_members SET Password='"+ pass +"' WHERE Id='" + data.Id + "'";
    db.query(q, data.data, function(err, result, fields) {
        if (err) {
            return callback({ "statuscode": "203", "status": "error", "msg": "Error occurered while updating password", "error": err });
        } else {
            if (result.affectedRows > 0) {
                return callback({ "statuscode": "200", "status": "success", "msg": "Password added successfully" });
            } else {
                return callback({ "statuscode": "203", "status": "warning", "msg": "Password not updated " });
            }
        }
    })        
}

module.exports.DeleteSelectedUser = function(data, uData, callback) {
    //console.log('ddtata',data);
    var q = "DELETE FROM team_members WHERE Id='" + data.Id + "'";
    db.query(q, data.data, function(err, result, fields) {
        if (err) {
            return callback({ "statuscode": "203", "status": "error", "msg": "Error occurered while deleting user", "error": err });
        } else {
            if (result.affectedRows > 0) {
                return callback({ "statuscode": "200", "status": "success", "msg": "User deleted successfully" });
            } else {
                return callback({ "statuscode": "203", "status": "warning", "msg": "User not deleted updated " });
            }
        }
    })        
}


module.exports.registerMentor = function(data, uData, callback) {
    q = "INSERT INTO `users` (`UserTypeId`, `UserName`, `Phone`, `Email`, `Password`, `JobPosition`, `IsActive`, `CreatedDate`) VALUES ('"+data.Role+"','"+data.Name+"','"+data.Phone+"','"+data.Email+"','"+md5(data.Password)+"','"+data.JobPosition+"',1,'"+ dateFormat(new Date(), 'yyyy-mm-dd 00:00:00') +"')";
    db.query(q, function(e, users, fields) {
            if (e) {
                return callback({
                    "statuscode": "203",
                    "status": "error",
                    "msg": 'something went wrong',
                    "error": e
                })
            } else {
                return callback({
                    "statuscode": "200",
                    "status": "success",
                    "msg": 'Mentor added successfully'
                })
            }
    })         
}

module.exports.allocateMentors = function(data, uData, callback) {
    var values = [];
    for(var i=0; i<data.length; i++){
        values.push([CompanyId= data[i].CompanyId, MentorId= data[i].MentorId])
    }
    q = "INSERT INTO `allocateMentor` (`CompanyId`, `MentorId`) VALUES ?";
    db.query(q, [values], function(e, users, fields) {
            if (e) {
                return callback({
                    "statuscode": "203",
                    "status": "error",
                    "msg": 'something went wrong',
                    "error": e
                })
            } else {
                return callback({
                    "statuscode": "200",
                    "status": "success",
                    "msg": 'Mentor allocated successfully'
                })
            }
    })         
}

module.exports.getUserProfileData = function(data, uData, callback) {
    if(data.Role == '1'){
        q="SELECT u.id,u.CompanyId,u.UserName AS Name,u.Email,u.JobPosition,u.Password,u.Phone,c.Address,c.FieldOfExpertise,c.NumberOfUsers,c.TIN FROM users u INNER JOIN company c ON c.id = u.CompanyId WHERE u.id = '"+data.UserId+"' AND u.IsActive = '1'";
    }else if(data.Role == '3'){
        q="SELECT * FROM users WHERE id = '"+data.UserId+"' AND IsActive = '1'";
    }else if(data.Role == '2'){
        q="SELECT * FROM users WHERE id = '"+data.UserId+"' AND IsActive = '1'";
    }

    db.query(q, function(err, users, fields) {
        if (err) {
            return callback({
                "statuscode": "203",
                "status": "error",
                "msg": 'something went wrong',
                "error": err
            })
        } else {
            return callback({
                "statuscode": "200",
                "status": "success",
                "msg": 'success',
                "data": users
            })
        }
    })         
}

module.exports.updateProfile = function(data, uData, callback) {
    if(data.Role == '1'){
        q="UPDATE users,company SET users.UserName='"+data.Name+"',company.Name='"+data.Name+"',users.Phone='"+data.Phone+"',company.Phone='"+data.Phone+"',company.FieldOfExpertise='"+data.FieldOfExpertise+"',company.Address='"+data.Address+"',company.NumberOfUsers='"+data.NumberOfUsers+"',company.TIN='"+data.TIN+"' WHERE users.CompanyId = company.id AND users.id='" + data.id + "'";
    }else if(data.Role == '3'){
        q="UPDATE users SET UserName='"+data.Name+"', Phone='"+data.Phone+"', JobPosition='"+data.JobPosition+"' WHERE id='" + data.id + "'";
    }else if(data.Role == '2'){
        q="UPDATE users SET UserName='"+data.Name+"', Phone='"+data.Phone+"', JobPosition='"+data.JobPosition+"' WHERE id='" + data.id + "'";
    }

    db.query(q, function(err, users, fields) {
        if (err) {
            return callback({
                "statuscode": "203",
                "status": "error",
                "msg": 'something went wrong',
                "error": err
            })
        } else {
            return callback({
                "statuscode": "200",
                "status": "success",
                "msg": 'success',
                "data": users
            })
        }
    })         
}

module.exports.changePassword = function(data, uData, callback) {

    q="SELECT * FROM users WHERE id='"+data.id+"' and Password= '"+md5(data.oldPassword)+"'";

    db.query(q, function(err, users, fields) {
        if (err) {
            return callback({
                "statuscode": "203",
                "status": "error",
                "msg": 'something went wrong',
                "error": err
            })
        } else {
            console.log(users.length,users);
            if(users.length == 0){
                return callback({
                    "statuscode": "203",
                    "status": "error",
                    "msg": 'Old Password in not matching'
                })
            }else{
                q="UPDATE users SET Password='"+md5(data.newPassword)+"' WHERE id='" + data.id + "'";
                db.query(q, function(err, users, fields) {
                    if (err) {
                        return callback({
                            "statuscode": "203",
                            "status": "error",
                            "msg": 'something went wrong',
                            "error": err
                        })
                    } else {
                        return callback({
                            "statuscode": "200",
                            "status": "success",
                            "msg": 'success',
                            "data": users
                        })
                    }
                })
            }
            
        }
    })
}


module.exports.getTeamsAndMembers = function(data, uData, callback) {
    q = "SELECT * FROM team WHERE CompanyId = '"+uData.CompanyId+"' AND IsActive = '1'";
    //console.log(q);
    db.query(q, function(err, teams, fields) {
        if (err) {
            return callback({
                "statuscode": "203",
                "status": "error",
                "msg": 'something went wrong',
                "error": err
            });
        } else {

            var teams_id = [];
            var members_id = [];
            var teamList = [];
            var default_team = '';


            if (teams.length < 1) {
                return callback({
                    "statuscode": "200",
                    "status": "success",
                    "data": { teams: teamList }
                })
            }



            teamList = teams;

            teams.forEach((t, index) => {
                teams_id.push(t.id)
                if (teamList[index].TeamType == 1) {
                    default_team = teamList[index];
                }
            });
            //console.log('default_team.UserId',default_team.UserId);
            getAdmin(default_team.UserId, (e, admin_details) => {
                if (err) {
                    return callback({
                        "statuscode": "203",
                        "status": "error",
                        "msg": 'something went wrong',
                        "error": err
                    })
                } else {

                    if (admin_details.length > 0) {

                        teamList.forEach((t, index) => {
                            teamList[index]['members'] = [];
                            if (t.TeamType == 1) {
                                teamList[index]['members'].push(admin_details[0]);
                                members_id.push(admin_details[0].Id)
                            }
                        });

                        getMembers(teams_id.join(','), (e, membersList) => {
                            //console.log('membersList',membersList);
                            if (e) {
                                return callback({
                                    "statuscode": "203",
                                    "status": "error",
                                    "msg": 'something went wrong',
                                    "error": e
                                })
                            } else {

                                // for (var i = 0; i < teamList.length; i++) {
                                //     for (var j = 0; j < teamList[i]['members'].length; j++) {
                                //         teamList[i]['members'][j]['plans'] = { done: [], plans: [], problems: [] };
                                //     }
                                // }

                                membersList.forEach((t, index) => {
                                    members_id.push(t.id)

                                });

                                for (var i = 0; i < teamList.length; i++) {
                                    for (var j = 0; j < membersList.length; j++) {

                                        if (teamList[i].id == membersList[j].TeamId) {

                                            teamList[i]['members'].push(membersList[j]);
                                            // teamList[i]['members'][j]['plans']={ done: [], plans: [], problems: [] };
                                        }

                                    }
                                }

                                // for (var i = 0; i < teamList.length; i++) {
                                //     for (var j = 0; j < teamList[i]['members'].length; j++) {
                                //         teamList[i]['members'][j]['plans'] = { done: [], plans: [], problems: [] };
                                //     }
                                // }
//console.log(teamList);
                                return callback({
                                    "statuscode": "200",
                                    "status": "success",
                                    "data": { teams: teamList }
                                });

                            }
                        })

                    }


                }
            });

        }
    })
}

module.exports.getCompanyObjective = function(data, uData, callback) {

    q = "SELECT * FROM objectives WHERE CompanyId='" + uData.CompanyId + "' AND TeamId='0' AND MemberId='0' AND DueYear='" + data.Year + "' AND DueQuarter='" + data.Quarter + "' AND IsActive = '1'";
    
    db.query(q, function(e, Objectives, fields) {
            if (e) {
                return callback({
                    "statuscode": "203",
                    "status": "error",
                    "msg": 'something went wrong',
                    "error": e
                })
            } else {
                var obj_ids = [];
                var okr_ids = [];
                var obj = [];

                if (Objectives.length < 1) {
                    return callback({
                        "statuscode": "200",
                        "status": "success",
                        "data": obj
                    })
                }

                obj = Objectives;
                for (var i = 0; i < obj.length; i++) {
                    obj_ids.push(obj[i].id);
                    obj[i]['comments'] = [];
                    obj[i]['okrs'] = [];
                    obj[i]['plan'] = { done: [], plans: [], problems: [] };

                }


                getOKRS(obj_ids.join(","), (e, OkrList) => {
                    if (e) {
                        return callback({
                            "statuscode": "203",
                            "status": "error",
                            "msg": 'something went wrong',
                            "error": e
                        })
                    } else {
                        var o_v = 0;
                        var okr = 0;
                        if (OkrList.length > 0) {
                            for (var i = 0; i < obj.length; i++) {
                                var t_v = 0;
                                for (var j = 0; j < OkrList.length; j++) {
                                    if (obj[i].id == OkrList[j].ObjectiveId) {
                                        t_v += parseInt(OkrList[j].Value, 10);
                                        obj[i]['okrs'].push(OkrList[j]);
                                        okr++;
                                        okr_ids.push(OkrList[j].id);
                                    }
                                }
                                if (obj[i]['okrs'].length > 0) {
                                    obj[i]['average_okr'] = Math.round(t_v / obj[i]['okrs'].length);
                                } else {
                                    obj[i]['average_okr'] = 0;
                                }
                                

                                if (obj[i]['average_okr'] >= 0) {
                                    o_v += parseInt(obj[i]['average_okr'], 10);
                                }
                            }
                        }
                        for (var i = 0; i < obj.length; i++) {
                            // console.log(obj[i]['average_okr'])
                        }
                        obj[0]['company_average'] = Math.round(o_v / obj.length);


                        q = "SELECT *,(SELECT DATEDIFF(DueDate,CURDATE())) AS Dues FROM plans WHERE LinkedToObj IN ("+obj_ids+")";
                        //console.log('qw',q);
                        var plansbox;
                        db.query(q, function(e, plans, fields) {
                            if (e) {
                                return callback({
                                    "statuscode": "203",
                                    "status": "error",
                                    "msg": 'something went wrong',
                                    "error": e
                                })
                            } else {
                                //console.log('pol',plans);
                                plansbox = plans;
                                /*obj['plan'] = { done: [], plans: [], problems: [] };
                                plans.forEach((p, p_i) => {
                                    //if (p.MemberId == memberId) {
                                        if (p.Type == 0) {
                                            obj['plan'].plans.push(p);
                                        } else if (p.Type == 1) {
                                            obj['plan'].done.push(p);
                                        } else {
                                            obj['plan'].problems.push(p);
                                        }
                                    //}
                                })*/
                                //console.log(obj['plan']);
                                for (var i = 0; i < plansbox.length; i++) {
                                    for (var j = 0; j < obj.length; j++) {
                                       if (obj[j].id == plansbox[i].LinkedToObj) {
                                            //obj[j]['comments'].push(CommentList[i]);
                                            if (plansbox[i].Type == 0) {
                                                obj[j]['plan'].plans.push(plansbox[i]);
                                            } else if (plansbox[i].Type == 1) {
                                                obj[j]['plan'].done.push(plansbox[i]);
                                            } else {
                                                obj[j]['plan'].problems.push(plansbox[i]);
                                            }
                                        }
                                    }
                                }
                            }
                        })

                        


                        getComments(obj_ids.join(","), okr_ids.join(","), (e, CommentList) => {

                            if (e) {
                                return callback({
                                    "statuscode": "203",
                                    "status": "error",
                                    "msg": 'something went wrong',
                                    "error": e
                                })
                            } else {
                                for (var i = 0; i < CommentList.length; i++) {
                                    for (var j = 0; j < obj.length; j++) {
                                        if (obj[j].id == CommentList[i].OkrId) {
                                            // console.log(CommentList[i].PostedBy)
                                            if (CommentList[i].PostedBy == 0) {
                                                CommentList[i].PostedBy = 'Admin'
                                            }

                                            obj[j]['comments'].push(CommentList[i]);
                                        }
                                        if (obj[j].id == CommentList[i].ObjectiveId) {
                                            // console.log(CommentList[i].PostedBy)
                                            if (CommentList[i].PostedBy == 0) {
                                                CommentList[i].PostedBy = 'Admin'
                                            }

                                            obj[j]['comments'].push(CommentList[i]);
                                        }
                                        for (var k = 0; k < obj[j]['okrs'].length; k++) {
                                            obj[j]['okrs'][k]['comments'] = []
                                        }
                                    }
                                }

                                for (var i = 0; i < CommentList.length; i++) {
                                    for (var j = 0; j < obj.length; j++) {
                                        // if (obj[j].Id == CommentList[i].ObjectiveId) {
                                        //     obj[j]['comments'].push(CommentList[i]);
                                        // }
                                        for (var k = 0; k < obj[j]['okrs'].length; k++) {
                                            // obj[j]['okrs'][k]['comments']=[]
                                            if (obj[j]['okrs'][k].id == CommentList[i].OkrId) {
                                                obj[j]['okrs'][k]['comments'].push(CommentList[i]);
                                            }
                                        }
                                    }
                                }
                                return callback({
                                    "statuscode": "200",
                                    "status": "success",
                                    "data": obj
                                })
                            }
                        });
                    }
                });

            }
    });
};


module.exports.getTeamMembersObjective = function(data, uData, callback) {
    q = "SELECT m.UserId as id,m.TeamId,u.UserName FROM members m INNER JOIN users u ON u.id = m.UserId WHERE TeamId = " + data.TeamId + " AND u.IsActive = '1'";
    db.query(q, function(e, members, fields) {
        if (e) {
            return callback({
                "statuscode": "203",
                "status": "error",
                "msg": 'something went wrong',
                "error": e
            })
        } else {
             //console.log(members);
             var members_ids =[];
            for (var i = 0; i < members.length; i++) {
                members_ids.push(members[i].id);
            }

            qry= "SELECT * FROM objectives WHERE CompanyId='" + uData.CompanyId + "' AND TeamId='" + data.TeamId + "' AND MemberId IN (" + members_ids + ") AND DueYear='" + data.Year + "' AND DueQuarter='" + data.Quarter + "' AND IsActive = '1'";
            //console.log(qry);
                db.query(qry, function(e, Objectives, fields) {
                if (e) {
                    return callback({
                        "statuscode": "203",
                        "status": "error",
                        "msg": 'something went wrong',
                        "error": e
                    })
                } else {
                    var obj_ids = [];
                    var okr_ids = [];
                    var obj = [];

                    if (Objectives.length < 1) {
                        return callback({
                            "statuscode": "200",
                            "status": "success",
                            "data": obj
                        })
                    }

                    obj = Objectives;
                    //console.log(Objectives);
                    for (var i = 0; i < obj.length; i++) {
                        obj_ids.push(obj[i].id);
                        obj[i]['comments'] = [];
                        obj[i]['okrs'] = [];
                        obj[i]['plan'] = { done: [], plans: [], problems: [] };
                    }

                    getOKRS(obj_ids.join(","), (e, OkrList) => {
                        if (e) {
                            return callback({
                                "statuscode": "203",
                                "status": "error",
                                "msg": 'something went wrong',
                                "error": e
                            })
                        } else {
                            var o_v = 0;
                            var okr = 0;
                            if (OkrList.length > 0) {
                                for (var i = 0; i < obj.length; i++) {
                                    var t_v = 0;
                                    for (var j = 0; j < OkrList.length; j++) {
                                        if (obj[i].id == OkrList[j].ObjectiveId) {
                                            t_v += parseInt(OkrList[j].Value, 10);
                                            obj[i]['okrs'].push(OkrList[j]);
                                            okr++;
                                            okr_ids.push(OkrList[j].id);
                                        }
                                    }
                                    if (obj[i]['okrs'].length > 0) {
                                        obj[i]['average_okr'] = Math.round(t_v / obj[i]['okrs'].length);
                                    } else {
                                        obj[i]['average_okr'] = 0;
                                    }


                                    if (obj[i]['average_okr'] >= 0) {
                                        o_v += parseInt(obj[i]['average_okr'], 10);
                                    }
                                }
                            }
                            for (var i = 0; i < obj.length; i++) {
                                // console.log(obj[i]['average_okr'])
                            }
                            obj[0]['company_average'] = Math.round(o_v / obj.length);
                            q = "SELECT *,(SELECT DATEDIFF(DueDate,CURDATE())) AS Dues FROM plans WHERE LinkedToObj IN ("+obj_ids+")";
                            //console.log('qw',q);
                            var plansbox;
                            db.query(q, function(e, plans, fields) {
                                if (e) {
                                    return callback({
                                        "statuscode": "203",
                                        "status": "error",
                                        "msg": 'something went wrong',
                                        "error": e
                                    })
                                } else {
                                    //console.log('pol',plans);
                                    plansbox = plans;
                                    
                                    for (var i = 0; i < plansbox.length; i++) {
                                        for (var j = 0; j < obj.length; j++) {
                                            if (obj[j].id == plansbox[i].LinkedToObj) {
                                                //obj[j]['comments'].push(CommentList[i]);
                                                if (plansbox[i].Type == 0) {
                                                    obj[j]['plan'].plans.push(plansbox[i]);
                                                } else if (plansbox[i].Type == 1) {
                                                    obj[j]['plan'].done.push(plansbox[i]);
                                                } else {
                                                    obj[j]['plan'].problems.push(plansbox[i]);
                                                }
                                            }
                                        }
                                    }
                                    //console.log(obj[j]['plan']);
                                }
                            })
                            getComments(obj_ids.join(","), okr_ids.join(","), (e, CommentList) => {

                                if (e) {
                                    return callback({
                                        "statuscode": "203",
                                        "status": "error",
                                        "msg": 'something went wrong',
                                        "error": e
                                    })
                                } else {
                                    for (var i = 0; i < CommentList.length; i++) {
                                        for (var j = 0; j < obj.length; j++) {
                                            if (obj[j].id == CommentList[i].OkrId) {
                                                // console.log(CommentList[i].PostedBy)
                                                if (CommentList[i].PostedBy == 0) {
                                                    CommentList[i].PostedBy = 'Admin'
                                                }

                                                obj[j]['comments'].push(CommentList[i]);
                                            }
                                            if (obj[j].id == CommentList[i].ObjectiveId) {
                                                // console.log(CommentList[i].PostedBy)
                                                if (CommentList[i].PostedBy == 0) {
                                                    CommentList[i].PostedBy = 'Admin'
                                                }

                                                obj[j]['comments'].push(CommentList[i]);
                                            }
                                            for (var k = 0; k < obj[j]['okrs'].length; k++) {
                                                obj[j]['okrs'][k]['comments'] = []
                                            }
                                        }
                                    }

                                    for (var i = 0; i < CommentList.length; i++) {
                                        for (var j = 0; j < obj.length; j++) {
                                            // if (obj[j].Id == CommentList[i].ObjectiveId) {
                                            //     obj[j]['comments'].push(CommentList[i]);
                                            // }
                                            for (var k = 0; k < obj[j]['okrs'].length; k++) {
                                                // obj[j]['okrs'][k]['comments']=[]
                                                if (obj[j]['okrs'][k].id == CommentList[i].OkrId) {
                                                    obj[j]['okrs'][k]['comments'].push(CommentList[i]);
                                                }
                                            }
                                        }
                                    }

                                    //members_ids
                                    var data = members;
                                    //data['members'] = [];
                                    //data['objectives'] = [];
                                    // members_ids

                                    for (var i = 0; i < data.length; i++) {
                                        data[i]['members'] = data[i].id;
                                        data[i]['objects'] = [];
                                    }
                                       
                                    for (var i = 0; i < data.length; i++) {
                                        for (var j = 0; j < obj.length; j++) {
                                            if (obj[j].MemberId == data[i].id) {
                                                //data['members'].push(members_ids[i]);
                                                data[i]['objects'].push(obj[j]);
                                            }
                                        }
                                    }
                                    console.log('datadata',data);
                                    /*members_ids.forEach((m,m_i) => {
                                        var d = [];
                                        data['members'].push(m);
                                        obj.forEach((o, o_i) => {
                                            if (o.MemberId == m) {
                                                //d.push(o);
                                                data['objectives'].push(o);
                                            }
                                        })
                                        
                                    });*/
                                           

                                    
                                    return callback({
                                        "statuscode": "200",
                                        "status": "success",
                                        "data": data
                                    })
                                }
                            });
                        }
                    });

                }
        });
        }
    })

};

module.exports.getMemberObjective = function(data, uData, callback) {
    q = "SELECT * FROM objectives WHERE CompanyId='" + uData.CompanyId + "' AND TeamId='" + data.TeamId + "' AND MemberId='" + data.MemberId + "' AND DueYear='" + data.Year + "' AND DueQuarter='" + data.Quarter + "' AND IsActive = '1'";
    db.query(q, function(e, Objectives, fields) {
            if (e) {
                return callback({
                    "statuscode": "203",
                    "status": "error",
                    "msg": 'something went wrong',
                    "error": e
                })
            } else {
                var obj_ids = [];
                var okr_ids = [];
                var obj = [];

                if (Objectives.length < 1) {
                    return callback({
                        "statuscode": "200",
                        "status": "success",
                        "data": obj
                    })
                }

                obj = Objectives;
                //console.log(Objectives);
                for (var i = 0; i < obj.length; i++) {
                    obj_ids.push(obj[i].id);
                    obj[i]['comments'] = [];
                    obj[i]['okrs'] = [];
                    obj[i]['plan'] = { done: [], plans: [], problems: [] };
                }

                getOKRS(obj_ids.join(","), (e, OkrList) => {
                    if (e) {
                        return callback({
                            "statuscode": "203",
                            "status": "error",
                            "msg": 'something went wrong',
                            "error": e
                        })
                    } else {
                        var o_v = 0;
                        var okr = 0;
                        if (OkrList.length > 0) {
                            for (var i = 0; i < obj.length; i++) {
                                var t_v = 0;
                                for (var j = 0; j < OkrList.length; j++) {
                                    if (obj[i].id == OkrList[j].ObjectiveId) {
                                        t_v += parseInt(OkrList[j].Value, 10);
                                        obj[i]['okrs'].push(OkrList[j]);
                                        okr++;
                                        okr_ids.push(OkrList[j].id);
                                    }
                                }
                                if (obj[i]['okrs'].length > 0) {
                                    obj[i]['average_okr'] = Math.round(t_v / obj[i]['okrs'].length);
                                } else {
                                    obj[i]['average_okr'] = 0;
                                }


                                if (obj[i]['average_okr'] >= 0) {
                                    o_v += parseInt(obj[i]['average_okr'], 10);
                                }
                            }
                        }
                        for (var i = 0; i < obj.length; i++) {
                            // console.log(obj[i]['average_okr'])
                        }
                        obj[0]['company_average'] = Math.round(o_v / obj.length);
                        q = "SELECT *,(SELECT DATEDIFF(DueDate,CURDATE())) AS Dues FROM plans WHERE LinkedToObj IN ("+obj_ids+")";
                        //console.log('qw',q);
                        var plansbox;
                        db.query(q, function(e, plans, fields) {
                            if (e) {
                                return callback({
                                    "statuscode": "203",
                                    "status": "error",
                                    "msg": 'something went wrong',
                                    "error": e
                                })
                            } else {
                                //console.log('pol',plans);
                                plansbox = plans;
                                /*obj['plan'] = { done: [], plans: [], problems: [] };
                                plans.forEach((p, p_i) => {
                                    //if (p.MemberId == memberId) {
                                        if (p.Type == 0) {
                                            obj['plan'].plans.push(p);
                                        } else if (p.Type == 1) {
                                            obj['plan'].done.push(p);
                                        } else {
                                            obj['plan'].problems.push(p);
                                        }
                                    //}
                                })*/
                                
                                for (var i = 0; i < plansbox.length; i++) {
                                    for (var j = 0; j < obj.length; j++) {
                                        if (obj[j].id == plansbox[i].LinkedToObj) {
                                            //obj[j]['comments'].push(CommentList[i]);
                                            if (plansbox[i].Type == 0) {
                                                obj[j]['plan'].plans.push(plansbox[i]);
                                            } else if (plansbox[i].Type == 1) {
                                                obj[j]['plan'].done.push(plansbox[i]);
                                            } else {
                                                obj[j]['plan'].problems.push(plansbox[i]);
                                            }
                                        }
                                    }
                                }
                                //console.log(obj[j]['plan']);
                            }
                        })
                        getComments(obj_ids.join(","), okr_ids.join(","), (e, CommentList) => {

                            if (e) {
                                return callback({
                                    "statuscode": "203",
                                    "status": "error",
                                    "msg": 'something went wrong',
                                    "error": e
                                })
                            } else {
                                for (var i = 0; i < CommentList.length; i++) {
                                    for (var j = 0; j < obj.length; j++) {
                                        if (obj[j].id == CommentList[i].OkrId) {
                                            // console.log(CommentList[i].PostedBy)
                                            if (CommentList[i].PostedBy == 0) {
                                                CommentList[i].PostedBy = 'Admin'
                                            }

                                            obj[j]['comments'].push(CommentList[i]);
                                        }
                                        if (obj[j].id == CommentList[i].ObjectiveId) {
                                            // console.log(CommentList[i].PostedBy)
                                            if (CommentList[i].PostedBy == 0) {
                                                CommentList[i].PostedBy = 'Admin'
                                            }

                                            obj[j]['comments'].push(CommentList[i]);
                                        }
                                        for (var k = 0; k < obj[j]['okrs'].length; k++) {
                                            obj[j]['okrs'][k]['comments'] = []
                                        }
                                    }
                                }

                                for (var i = 0; i < CommentList.length; i++) {
                                    for (var j = 0; j < obj.length; j++) {
                                        // if (obj[j].Id == CommentList[i].ObjectiveId) {
                                        //     obj[j]['comments'].push(CommentList[i]);
                                        // }
                                        for (var k = 0; k < obj[j]['okrs'].length; k++) {
                                            // obj[j]['okrs'][k]['comments']=[]
                                            if (obj[j]['okrs'][k].id == CommentList[i].OkrId) {
                                                obj[j]['okrs'][k]['comments'].push(CommentList[i]);
                                            }
                                        }
                                    }
                                }
                                //console.log('objok',obj);
                                return callback({
                                    "statuscode": "200",
                                    "status": "success",
                                    "data": obj
                                })
                            }
                        });
                    }
                });

            }
    });
};

module.exports.getTeamObjective = function(data, uData, callback) {
    //console.log(data, uData);
    q = "SELECT * FROM objectives WHERE CompanyId='" + uData.CompanyId + "' AND TeamId='" + data.TeamId + "' AND MemberId='0' AND DueYear='" + data.Year + "' AND DueQuarter='" + data.Quarter + "' AND IsActive = '1'";
    //console.log('q',q);
    db.query(q, function(e, Objectives, fields) {
            if (e) {
                return callback({
                    "statuscode": "203",
                    "status": "error",
                    "msg": 'something went wrong',
                    "error": e
                })
            } else {
                var obj_ids = [];
                var okr_ids = [];
                var obj = [];

                if (Objectives.length < 1) {
                    return callback({
                        "statuscode": "200",
                        "status": "success",
                        "data": obj
                    })
                }

                obj = Objectives;
                for (var i = 0; i < obj.length; i++) {
                    obj_ids.push(obj[i].id);
                    obj[i]['comments'] = [];
                    obj[i]['okrs'] = [];
                    obj[i]['plan'] = { done: [], plans: [], problems: [] };
                }

                getOKRS(obj_ids.join(","), (e, OkrList) => {
                    if (e) {
                        return callback({
                            "statuscode": "203",
                            "status": "error",
                            "msg": 'something went wrong',
                            "error": e
                        })
                    } else {
                        var o_v = 0;
                        var okr = 0;
                        if (OkrList.length > 0) {
                            for (var i = 0; i < obj.length; i++) {
                                var t_v = 0;
                                for (var j = 0; j < OkrList.length; j++) {
                                    if (obj[i].id == OkrList[j].ObjectiveId) {
                                        t_v += parseInt(OkrList[j].Value, 10);
                                        obj[i]['okrs'].push(OkrList[j]);
                                        okr++;
                                        okr_ids.push(OkrList[j].id);
                                    }
                                }
                                if (obj[i]['okrs'].length > 0) {
                                    obj[i]['average_okr'] = Math.round(t_v / obj[i]['okrs'].length);
                                } else {
                                    obj[i]['average_okr'] = 0;
                                }


                                if (obj[i]['average_okr'] >= 0) {
                                    o_v += parseInt(obj[i]['average_okr'], 10);
                                }
                            }
                        }
                        for (var i = 0; i < obj.length; i++) {
                            // console.log(obj[i]['average_okr'])
                        }
                        obj[0]['company_average'] = Math.round(o_v / obj.length);
                        q = "SELECT *,(SELECT DATEDIFF(DueDate,CURDATE())) AS Dues FROM plans WHERE LinkedToObj IN ("+obj_ids+")";
                        //console.log('qw',q);
                        var plansbox;
                        db.query(q, function(e, plans, fields) {
                            if (e) {
                                return callback({
                                    "statuscode": "203",
                                    "status": "error",
                                    "msg": 'something went wrong',
                                    "error": e
                                })
                            } else {
                                //console.log('pol',plans);
                                plansbox = plans;
                                /*obj['plan'] = { done: [], plans: [], problems: [] };
                                plans.forEach((p, p_i) => {
                                    //if (p.MemberId == memberId) {
                                        if (p.Type == 0) {
                                            obj['plan'].plans.push(p);
                                        } else if (p.Type == 1) {
                                            obj['plan'].done.push(p);
                                        } else {
                                            obj['plan'].problems.push(p);
                                        }
                                    //}
                                })*/
                                //console.log(obj['plan']);
                                for (var i = 0; i < plansbox.length; i++) {
                                    for (var j = 0; j < obj.length; j++) {
                                       if (obj[j].id == plansbox[i].LinkedToObj) {
                                            //obj[j]['comments'].push(CommentList[i]);
                                            if (plansbox[i].Type == 0) {
                                                obj[j]['plan'].plans.push(plansbox[i]);
                                            } else if (plansbox[i].Type == 1) {
                                                obj[j]['plan'].done.push(plansbox[i]);
                                            } else {
                                                obj[j]['plan'].problems.push(plansbox[i]);
                                            }
                                        }
                                    }
                                }
                            }
                        })
                        getComments(obj_ids.join(","), okr_ids.join(","), (e, CommentList) => {

                            if (e) {
                                return callback({
                                    "statuscode": "203",
                                    "status": "error",
                                    "msg": 'something went wrong',
                                    "error": e
                                })
                            } else {
                                for (var i = 0; i < CommentList.length; i++) {
                                    for (var j = 0; j < obj.length; j++) {
                                        if (obj[j].id == CommentList[i].OkrId) {
                                            // console.log(CommentList[i].PostedBy)
                                            if (CommentList[i].PostedBy == 0) {
                                                CommentList[i].PostedBy = 'Admin'
                                            }

                                            obj[j]['comments'].push(CommentList[i]);
                                        }
                                        if (obj[j].id == CommentList[i].ObjectiveId) {
                                            // console.log(CommentList[i].PostedBy)
                                            if (CommentList[i].PostedBy == 0) {
                                                CommentList[i].PostedBy = 'Admin'
                                            }

                                            obj[j]['comments'].push(CommentList[i]);
                                        }
                                        for (var k = 0; k < obj[j]['okrs'].length; k++) {
                                            obj[j]['okrs'][k]['comments'] = []
                                        }
                                    }
                                }

                                for (var i = 0; i < CommentList.length; i++) {
                                    for (var j = 0; j < obj.length; j++) {
                                        // if (obj[j].Id == CommentList[i].ObjectiveId) {
                                        //     obj[j]['comments'].push(CommentList[i]);
                                        // }
                                        for (var k = 0; k < obj[j]['okrs'].length; k++) {
                                            // obj[j]['okrs'][k]['comments']=[]
                                            if (obj[j]['okrs'][k].id == CommentList[i].OkrId) {
                                                obj[j]['okrs'][k]['comments'].push(CommentList[i]);
                                            }
                                        }
                                    }
                                }
                                return callback({
                                    "statuscode": "200",
                                    "status": "success",
                                    "data": obj
                                })
                            }
                        });
                    }
                });

            }
    });
};

module.exports.getCompanyGraph = function(data, uData, callback) {
    q = "SELECT kr.KeyResultId, kr.ProgressValue, krs.ObjectiveId, DATEDIFF(kr.CreatedDate, MAKEDATE(YEAR(CURDATE()), 1) + INTERVAL QUARTER(CURDATE()) QUARTER - INTERVAL 1 QUARTER) DIV 7 AS weekNumber, MAKEDATE(YEAR(CURDATE()), 1) + INTERVAL QUARTER(CURDATE()) QUARTER - INTERVAL 1 QUARTER + INTERVAL (DATEDIFF(kr.CreatedDate, MAKEDATE(YEAR(CURDATE()), 1) + INTERVAL QUARTER(CURDATE()) QUARTER - INTERVAL 1 QUARTER) DIV 7) WEEK AS week_start_date, SUM(kr.ProgressValue) AS total, COUNT(*) AS transactions, AVG(kr.ProgressValue) AS average FROM keyresultcheckin kr INNER JOIN `keyresults` krs ON krs.id = kr.KeyResultId INNER JOIN `objectives` obj ON obj.id = krs.ObjectiveId WHERE obj.CompanyId='" + uData.CompanyId + "' AND obj.TeamId='0' AND obj.MemberId = '0' AND  obj.DueYear='" + data.Year + "' AND  obj.DueQuarter='" + data.Quarter + "'";
    db.query(q, function(e, graph, fields) {
        if (e) {
            return callback({
                "statuscode": "203",
                "status": "error",
                "msg": 'something went wrong',
                "error": e
            })
        } else {
            return callback({
                "statuscode": "200",
                "status": "success",
                "data": graph
            })
        }
    })

};

module.exports.getTeamGraph = function(data, uData, callback) {
    q = "SELECT kr.KeyResultId, kr.ProgressValue, krs.ObjectiveId, DATEDIFF(kr.CreatedDate, MAKEDATE(YEAR(CURDATE()), 1) + INTERVAL QUARTER(CURDATE()) QUARTER - INTERVAL 1 QUARTER) DIV 7 AS weekNumber, MAKEDATE(YEAR(CURDATE()), 1) + INTERVAL QUARTER(CURDATE()) QUARTER - INTERVAL 1 QUARTER + INTERVAL (DATEDIFF(kr.CreatedDate, MAKEDATE(YEAR(CURDATE()), 1) + INTERVAL QUARTER(CURDATE()) QUARTER - INTERVAL 1 QUARTER) DIV 7) WEEK AS week_start_date, SUM(kr.ProgressValue) AS total, COUNT(*) AS transactions, AVG(kr.ProgressValue) AS average FROM keyresultcheckin kr INNER JOIN `keyresults` krs ON krs.id = kr.KeyResultId INNER JOIN `objectives` obj ON obj.id = krs.ObjectiveId WHERE obj.CompanyId='" + uData.CompanyId + "' AND obj.TeamId='" + data.TeamId + "' AND obj.MemberId = '0' AND  obj.DueYear='" + data.Year + "' AND  obj.DueQuarter='" + data.Quarter + "'";
    db.query(q, function(e, graph, fields) {
        if (e) {
            return callback({
                "statuscode": "203",
                "status": "error",
                "msg": 'something went wrong',
                "error": e
            })
        } else {
            return callback({
                "statuscode": "200",
                "status": "success",
                "data": graph
            })
        }
    })

};

module.exports.getMemberGraph = function(data, uData, callback) {
    q = "SELECT kr.KeyResultId, kr.ProgressValue, krs.ObjectiveId, DATEDIFF(kr.CreatedDate, MAKEDATE(YEAR(CURDATE()), 1) + INTERVAL QUARTER(CURDATE()) QUARTER - INTERVAL 1 QUARTER) DIV 7 AS weekNumber, MAKEDATE(YEAR(CURDATE()), 1) + INTERVAL QUARTER(CURDATE()) QUARTER - INTERVAL 1 QUARTER + INTERVAL (DATEDIFF(kr.CreatedDate, MAKEDATE(YEAR(CURDATE()), 1) + INTERVAL QUARTER(CURDATE()) QUARTER - INTERVAL 1 QUARTER) DIV 7) WEEK AS week_start_date, SUM(kr.ProgressValue) AS total, COUNT(*) AS transactions, AVG(kr.ProgressValue) AS average FROM keyresultcheckin kr INNER JOIN `keyresults` krs ON krs.id = kr.KeyResultId INNER JOIN `objectives` obj ON obj.id = krs.ObjectiveId WHERE obj.CompanyId='" + uData.CompanyId + "' AND obj.TeamId='" + data.TeamId + "' AND obj.MemberId = '" + data.MemberId + "' AND  obj.DueYear='" + data.Year + "' AND  obj.DueQuarter='" + data.Quarter + "'";
    db.query(q, function(e, graph, fields) {
        if (e) {
            return callback({
                "statuscode": "203",
                "status": "error",
                "msg": 'something went wrong',
                "error": e
            })
        } else {
            return callback({
                "statuscode": "200",
                "status": "success",
                "data": graph
            })
        }
    })

};


module.exports.addWeekComment = function(data, uData, callback) {
    //console.log(data);
    q = "INSERT INTO `weekly_checkins` (`ObjectiveId`, `CompanyId`, `TeamId`, `MemberId`, `ProgressValue`, `Comment`,`CommentType`, `Plan`, `CreatedBy`, `CreatedDate`) VALUES ('0','"+data.CompanyId+"','"+data.TeamId+"','"+data.MemberId+"','"+data.ProgressValue+"','"+data.Comment+"','"+data.CommentType+"','"+data.Plan+"','"+data.CreatedBy+"','"+ dateFormat(new Date(), 'yyyy-mm-dd') +"')";
    //console.log(q);
    db.query(q, function(e, commentplan, fields) {
            if (e) {
                return callback({
                    "statuscode": "203",
                    "status": "error",
                    "msg": 'something went wrong',
                    "error": e
                })
            } else {
                return callback({
                    "statuscode": "200",
                    "status": "success",
                    "msg": 'Created successfully',
                    "data": commentplan
                })
            }
    })
}

module.exports.getCompanyObjectiveWeek = function(data, uData, callback) {
    q = "SELECT w.id,ob.ObjectiveName,ROUND(SUM(w.ProgressValue)/COUNT(w.id),1) AS Avg FROM weekly_checkins  w INNER JOIN `objectives` ob ON ob.id = w.ObjectiveId WHERE w.CompanyId='" + uData.CompanyId + "' AND w.TeamId='0' AND w.MemberId='0' AND DATE(w.UpdatedDate)>=DATE('" + data.FromDate + "') AND DATE(w.UpdatedDate)<=DATE('" + data.ToDate + "') AND w.ProgressValue != '0' GROUP BY w.ObjectiveId";

    //console.log(q);
    db.query(q, function(e, Objectives, fields) {
        if (e) {
            return callback({
                "statuscode": "203",
                "status": "error",
                "msg": 'something went wrong',
                "error": e
            })
        } else {
            
            var result = {objective:[],comment:[],plan:[]};
            result.objective.push(Objectives);
            qs = "SELECT * FROM weekly_checkins WHERE CompanyId='" + uData.CompanyId + "' AND TeamId='0' AND MemberId='0' AND DATE(UpdatedDate)>=DATE('" + data.FromDate + "') AND DATE(UpdatedDate)<=DATE('" + data.ToDate + "') AND ObjectiveId = '0'";

            db.query(qs, function(e, Comm, fields) {
                if (e) {
                    return callback({
                        "statuscode": "203",
                        "status": "error",
                        "msg": 'something went wrong',
                        "error": e
                    })
                } else {

                    for(var i=0;i<Comm.length;i++){
                        if(Comm[i].Comment != ''){
                            result.comment.push(Comm[i]);
                        }else{
                            result.plan.push(Comm[i]);
                        }
                    }
                    //console.log('re',result);
                    return callback({
                        "statuscode": "200",
                        "status": "success",
                        "data": result
                    })
                }
            })

            
        }
    });
};

module.exports.getMemberObjectiveWeek = function(data, uData, callback) {
    q = "SELECT w.id,w.ObjectiveId,ob.ObjectiveName,ROUND(SUM(w.ProgressValue)/COUNT(w.id),1) AS Avg FROM weekly_checkins  w INNER JOIN `objectives` ob ON ob.id = w.ObjectiveId WHERE w.CompanyId='" + uData.CompanyId + "' AND w.TeamId='" + data.TeamId + "' AND w.MemberId='" + data.MemberId + "' AND DATE(w.UpdatedDate)>=DATE('" + data.FromDate + "') AND DATE(w.UpdatedDate)<=DATE('" + data.ToDate + "') AND w.ProgressValue != '0' GROUP BY w.ObjectiveId";
    db.query(q, function(e, obj, fields) {
        if (e) {
            return callback({
                "statuscode": "203",
                "status": "error",
                "msg": 'something went wrong',
                "error": e
            })
        } else {
            
            var obj_ids = [];
            obj.forEach((p, i) => {
                obj_ids.push(p.ObjectiveId);
            });

            var qqqq = [];
            if (obj_ids.length > 0) {
                qqqq.push("id NOT IN (" + obj_ids.join(',') + ")");
            } else {
                qqqq.push("id NOT IN (0)");
            }

            q = "SELECT id, id AS ObjectiveId,ObjectiveName, 0 AS Avg FROM objectives WHERE CompanyId='" + uData.CompanyId + "' AND TeamId='" + data.TeamId + "' AND MemberId='" + data.MemberId + "' AND " + qqqq.join(' OR ');
            //console.log(q);
            db.query(q, function(e, MainObj, fields) {
                if (e) {
                    return callback({
                        "statuscode": "203",
                        "status": "error",
                        "msg": 'something went wrong',
                        "error": e
                    })
                } else {
                    
                    var result = {objective:[],comment:[{done:[],risk:[],dependencies:[]}],plan:[]};
                    var finalOBjectives = obj.concat(MainObj);
                    result.objective.push(finalOBjectives);
                    //console.log(obj.concat(MainObj));
                    qs = "SELECT * FROM weekly_checkins WHERE CompanyId='" + uData.CompanyId + "' AND TeamId='" + data.TeamId + "' AND MemberId='" + data.MemberId + "' AND DATE(UpdatedDate)>=DATE('" + data.FromDate + "') AND DATE(UpdatedDate)<=DATE('" + data.ToDate + "') AND ObjectiveId = '0'";
                    //console.log(qs);
                    db.query(qs, function(e, Comm, fields) {
                        if (e) {
                            return callback({
                                "statuscode": "203",
                                "status": "error",
                                "msg": 'something went wrong',
                                "error": e
                            })
                        } else {

                            for(var i=0;i<Comm.length;i++){
                                if(Comm[i].Comment != ''){
                                    if(Comm[i].CommentType == '0'){
                                        result.comment[0].done.push(Comm[i]);
                                    }else if(Comm[i].CommentType == '1'){
                                        result.comment[0].risk.push(Comm[i]);
                                    }else if(Comm[i].CommentType == '2'){
                                        result.comment[0].dependencies.push(Comm[i]);
                                    }
                                    //result.comment.push(Comm[i]);
                                }else{
                                    result.plan.push(Comm[i]);
                                }
                            }
                            //console.log('re',result);
                            return callback({
                                "statuscode": "200",
                                "status": "success",
                                "data": result
                            })
                        }
                    })
                }
            })
        }
    })
};

module.exports.getTeamObjectiveWeek = function(data, uData, callback) {
    q = "SELECT w.id,ob.ObjectiveName,ROUND(SUM(w.ProgressValue)/COUNT(w.id),1) AS Avg FROM weekly_checkins  w INNER JOIN `objectives` ob ON ob.id = w.ObjectiveId WHERE w.CompanyId='" + uData.CompanyId + "' AND w.TeamId='" + data.TeamId + "' AND w.MemberId='0' AND DATE(w.UpdatedDate)>=DATE('" + data.FromDate + "') AND DATE(w.UpdatedDate)<=DATE('" + data.ToDate + "') AND w.ProgressValue != '0' GROUP BY w.ObjectiveId";

    //console.log(q);
    db.query(q, function(e, Objectives, fields) {
        if (e) {
            return callback({
                "statuscode": "203",
                "status": "error",
                "msg": 'something went wrong',
                "error": e
            })
        } else {
            
            var result = {objective:[],comment:[],plan:[]};
            result.objective.push(Objectives);
            qs = "SELECT * FROM weekly_checkins WHERE CompanyId='" + uData.CompanyId + "' AND TeamId='" + data.TeamId + "' AND MemberId='0' AND DATE(UpdatedDate)>=DATE('" + data.FromDate + "') AND DATE(UpdatedDate)<=DATE('" + data.ToDate + "') AND ObjectiveId = '0'";

            db.query(qs, function(e, Comm, fields) {
                if (e) {
                    return callback({
                        "statuscode": "203",
                        "status": "error",
                        "msg": 'something went wrong',
                        "error": e
                    })
                } else {

                    for(var i=0;i<Comm.length;i++){
                        if(Comm[i].Comment != ''){
                            result.comment.push(Comm[i]);
                        }else{
                            result.plan.push(Comm[i]);
                        }
                    }
                    //console.log('re',result);
                    return callback({
                        "statuscode": "200",
                        "status": "success",
                        "data": result
                    })
                }
            })

            
        }
    });
};




module.exports.getObjectiveById = function(data, uData, callback) {

    q = "SELECT * FROM objectives WHERE id='" + data.id + "' AND IsActive = '1'";
    
    db.query(q, function(e, Objectives, fields) {
            if (e) {
                return callback({
                    "statuscode": "203",
                    "status": "error",
                    "msg": 'something went wrong',
                    "error": e
                })
            } else {
                var obj_ids = [];
                var okr_ids = [];
                var obj = [];

                if (Objectives.length < 1) {
                    return callback({
                        "statuscode": "200",
                        "status": "success",
                        "data": obj
                    })
                }

                obj = Objectives;
                for (var i = 0; i < obj.length; i++) {
                    obj_ids.push(obj[i].id);
                    obj[i]['comments'] = [];
                    obj[i]['okrs'] = [];
                }

                getOKRS(obj_ids.join(","), (e, OkrList) => {
                    if (e) {
                        return callback({
                            "statuscode": "203",
                            "status": "error",
                            "msg": 'something went wrong',
                            "error": e
                        })
                    } else {
                        var o_v = 0;
                        var okr = 0;
                        if (OkrList.length > 0) {
                            for (var i = 0; i < obj.length; i++) {
                                var t_v = 0;
                                for (var j = 0; j < OkrList.length; j++) {
                                    if (obj[i].id == OkrList[j].ObjectiveId) {
                                        t_v += parseInt(OkrList[j].Value, 10);
                                        obj[i]['okrs'].push(OkrList[j]);
                                        okr++;
                                        okr_ids.push(OkrList[j].id);
                                    }
                                }
                                if (obj[i]['okrs'].length > 0) {
                                    obj[i]['average_okr'] = Math.round(t_v / obj[i]['okrs'].length);
                                } else {
                                    obj[i]['average_okr'] = 0;
                                }


                                if (obj[i]['average_okr'] >= 0) {
                                    o_v += parseInt(obj[i]['average_okr'], 10);
                                }
                            }
                        }
                        for (var i = 0; i < obj.length; i++) {
                            // console.log(obj[i]['average_okr'])
                        }
                        obj[0]['company_average'] = Math.round(o_v / obj.length);
                        getComments(obj_ids.join(","), okr_ids.join(","), (e, CommentList) => {

                            if (e) {
                                return callback({
                                    "statuscode": "203",
                                    "status": "error",
                                    "msg": 'something went wrong',
                                    "error": e
                                })
                            } else {
                                for (var i = 0; i < CommentList.length; i++) {
                                    for (var j = 0; j < obj.length; j++) {
                                        if (obj[j].id == CommentList[i].OkrId) {
                                            // console.log(CommentList[i].PostedBy)
                                            if (CommentList[i].PostedBy == 0) {
                                                CommentList[i].PostedBy = 'Admin'
                                            }

                                            obj[j]['comments'].push(CommentList[i]);
                                        }
                                        for (var k = 0; k < obj[j]['okrs'].length; k++) {
                                            obj[j]['okrs'][k]['comments'] = []
                                        }
                                    }
                                }

                                for (var i = 0; i < CommentList.length; i++) {
                                    for (var j = 0; j < obj.length; j++) {
                                        // if (obj[j].Id == CommentList[i].ObjectiveId) {
                                        //     obj[j]['comments'].push(CommentList[i]);
                                        // }
                                        for (var k = 0; k < obj[j]['okrs'].length; k++) {
                                            // obj[j]['okrs'][k]['comments']=[]
                                            if (obj[j]['okrs'][k].id == CommentList[i].OkrId) {
                                                obj[j]['okrs'][k]['comments'].push(CommentList[i]);
                                            }
                                        }
                                    }
                                }
                                return callback({
                                    "statuscode": "200",
                                    "status": "success",
                                    "data": obj
                                })
                            }
                        });
                    }
                });

            }
    });
};


module.exports.getUserById = function(data, uData, callback) {

    q = "SELECT * FROM objectives WHERE id='" + data.id + "' AND IsActive = '1'";

    db.query(q, function(e, Objectives, fields) {
            if (e) {
                return callback({
                    "statuscode": "203",
                    "status": "error",
                    "msg": 'something went wrong',
                    "error": e
                })
            } else {
                //console.log(Objectives[0].ObjectiveType);
                if(Objectives[0].ObjectiveType == '1'){
                    q = "SELECT * FROM company WHERE id='" + Objectives[0].CompanyId + "' AND IsActive = '1'";
                }else if(Objectives[0].ObjectiveType == '2'){
                    q = "SELECT * FROM team WHERE id='" + Objectives[0].TeamId + "' AND IsActive = '1'";
                }else{
                    q = "SELECT * FROM users WHERE id='" + Objectives[0].MemberId + "' AND IsActive = '1'";
                }

                db.query(q, function(e, users, fields) {
                    if (e) {
                        return callback({
                            "statuscode": "203",
                            "status": "error",
                            "msg": 'something went wrong',
                            "error": e
                        })
                    } else {
                        users[0]['userType'] = Objectives[0].ObjectiveType;
                        return callback({
                            "statuscode": "200",
                            "status": "success",
                            "data": users
                        })
                    }
                })
            }
    })

}


module.exports.getLinkData = (data, uData, callback) => {

    var q = "SELECT id,UserId FROM team WHERE CompanyId='" + uData.CompanyId + "'";

    //console.log(q,data.Item);
    db.query(q, function(err, tms, fields) {
        var user_id = tms[0].UserId;
        var team_ids = [];
        for(var i=0;i<tms.length;i++){
            team_ids.push(tms[i].id);
        }

        if (data.Type == 'obj_link') {
            getLinkObjectives(data, (d) => {
                return callback(d);
            });
        }
        if (data.Type == 'okr_assign') {
            q = 'SELECT * FROM objectives WHERE id="' + data.Item.ObjectiveId + '"';
            db.query(q, function(err, obj, fields) {
                data.Item = obj[0];
                getCompanyList(data, (d) => {
                    return callback(d);
                });
            });
        }
        if (data.Type == 'link_plan') {
            q = "SELECT o.*,lp.PlanId FROM objectives o " +
                "LEFT JOIN linked_plans lp ON o.id=lp.ObjectiveId AND lp.PlanId='" + data.Item.id + "' "+
                "WHERE CompanyId='"+uData.CompanyId+"'";
            // "LEFT JOIN plans o ON 0.Id=lp.ObjectiveId "+
            // "WHERE Id='" + data.Item.ObjectiveId + "'";
            //console.log(q);
            db.query(q, function(err, obj, fields) {
                // var s_d = { linked: [], unlinked: [] };
                var d = { personal: { linked: [], unlinked: [] }, company: { linked: [], unlinked: [] }, teams: { linked: [], unlinked: [] } };
                var totalLinked = 0;
                obj.forEach(function(element, index) {
                    var t = '';
                    if (element.ObjectiveType  == 1) {
                        t = 'company';
                    } else if (element.ObjectiveType  == 3) {
                        t = 'personal';
                    } else {
                        t = 'teams';
                    }
                    // console.log(t);

                    if (element.PlanId) {
                        d[t].linked.push(element);
                        totalLinked++;
                        d['totalLinked'] = totalLinked;
                    } else {
                        d[t].unlinked.push(element);
                    }
                });

                return callback({
                    "statuscode": "200",
                    "status": "success",
                    "data": d
                })
                // });
            });

            // else{
            //     getTeams();
            // }
        }
    });
}